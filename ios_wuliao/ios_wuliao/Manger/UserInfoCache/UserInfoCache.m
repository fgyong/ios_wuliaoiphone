//
//  UserInfoCache.m
//  BLTSZY
//
//  Created by Charlie on 2017/5/22.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "UserInfoCache.h"

 



@implementation UserInfoCache

+(instancetype)share{
    static dispatch_once_t onceToken;
    static UserInfoCache *_user;
    dispatch_once(&onceToken, ^{
        _user =[UserInfoCache new];
    });
    return _user;
}
-(NSDictionary *)objForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}
-(void)setUserInfo:(NSDictionary *)user forKey:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setObject:user forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];//同步数据
}
-(NSDictionary *)userInfo {
    return  [[NSUserDefaults standardUserDefaults] objectForKey:kUserInfoKey];
}
-(BOOL)expire{
    return YES;
}
-(NSString *)name{
    return [self objForKey:@"userName"] ? [self objForKey:@"userName"][@"cUserName"] : @"";
}
-(NSString *)userCode{
    return [self containsObj:@"cUserCode"] ? [self userInfo][@"cUserCode"] : @"";
}
-(NSString *)Accid{
    return [self containsObj:@"cAccid"] ? [self userInfo][@"cAccid"]: @"";
}
-(void)setLocalNamePwd:(NSDictionary *)dic{
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:kUserInfoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];//同步数据
}
-(NSArray *)allkeys{
    return  [[self userInfo] allKeys];
}
-(BOOL)containsObj:(NSString *)obj{
    return [[self allkeys] containsObject:obj];
}
-(void)clearCache{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserInfoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];//同步数据
}
@end
