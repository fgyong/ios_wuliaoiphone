//
//  UserInfoCache.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/22.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <Foundation/Foundation.h>
static NSString * kUserInfoKey = @"userInfoKey";
@interface UserInfoCache : NSObject

+(instancetype)share;
/**
 设置本地user

 @param user nil
 */
-(void)setUserInfo:(NSDictionary *)user forKey:(NSString *)key;

/**
 获取userinif

 @return 返回user 字典
 */
-(NSDictionary *)userInfo;
-(NSDictionary *)objForKey:(NSString *)key;
/**
 账户是否过期

 @return 返回yes 过期 否则 不过期
 */
-(BOOL)expire;
-(NSString *)name;
-(NSString *)userCode;
-(NSString *)Accid;
-(void)clearCache;
-(void)setLocalNamePwd:(NSDictionary *)dic;



@end
