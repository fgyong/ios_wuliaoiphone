//
//  FYKeyBoardManger.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/23.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIViewController+MMD.h"

@interface FYKeyBoardManger : NSObject

/**
 检测键盘 当键盘出现的时候 出现灰色遮罩

 @return return instancetype
 */
+(instancetype)shareManger;

/**
 设置可用不可用

 @param valible yes 可用
 */
-(void)setValible:(BOOL)valible;

- (UIViewController *)visibleVC;
@end
