//
//  FYKeyBoardManger.m
//  BLTSZY
//
//  Created by Charlie on 2017/5/23.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYKeyBoardManger.h"

@interface FYKeyBoardManger ()

@property (nonnull,nonatomic,strong) UIView * view;
@property (nonatomic,assign) BOOL isValible;
@end

@implementation FYKeyBoardManger
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
+(instancetype)shareManger{
    static dispatch_once_t onceToken;
    static FYKeyBoardManger *_keyboardManger;
    dispatch_once(&onceToken, ^{
        _keyboardManger=[FYKeyBoardManger new];
        [_keyboardManger initObj];
    });
    return _keyboardManger;
}
-(void)initObj{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}
-(UIView *)view{
    if (_view == nil) {
        _view=[UIView new];
        _view.backgroundColor = [UIColor blackColor];
        _view.alpha = 0.3;
        _view.frame = [UIApplication sharedApplication] .keyWindow.bounds;
        UITapGestureRecognizer * pin =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeFromSuperview)];
        [_view addGestureRecognizer:pin];
       // self.isValible = NO;
    }
    return _view;
}
-(void)keyBoardFrameChange:(NSNotification *)obj{
    if (self.isValible == NO) {
        return;
    }
    NSString * name = [obj name];
    UIViewController * vc=[self visibleVC];
    if ([name isEqualToString:UIKeyboardDidShowNotification]) {
        if (NO == [vc.view.subviews containsObject:self.view]) {
            self.view.frame = vc.view.bounds;
            self.view.alpha = 0.0f;
            [vc.view addSubview:self.view];
            [UIView animateWithDuration:0.25 animations:^{
                 self.view.alpha = 0.3f;
            }];
        }
    } else if([name isEqualToString:UIKeyboardWillHideNotification]){
        [self.view removeFromSuperview];
    }
}
-(void)setValible:(BOOL)valible{
    self.isValible = valible;
}
-(void)removeFromSuperview{
    [self.view removeFromSuperview];
    [[self visibleVC].view endEditing:YES];
}
-(UIViewController *)visibleVC{
        AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    UIViewController * vc = [appdelegate visiableVC];
    if ([vc isKindOfClass:[UIViewController class]]) {
        if ([vc isKindOfClass:[UINavigationController class]]) {
            UINavigationController * NaVc=(UINavigationController *)vc;
            return [NaVc visibleViewController];
        }
        return vc;
    } else if([vc isKindOfClass:[UINavigationController class]])
    {
        UINavigationController * na = (UINavigationController *)vc;
       return  [na visibleViewController];
    }
    return vc;
}
@end
