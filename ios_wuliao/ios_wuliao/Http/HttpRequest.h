//
//  HttpRequest.h
//  BLTSZY
//
//  Created by Charlie on 2017/3/29.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <Foundation/Foundation.h>

//Block
typedef void(^NetStartHander)(NSProgress *  downloadProgress); // 发起请求
typedef void(^NetSuccessHander)(NSDictionary *  result);    // 成功返回
typedef void(^NetFailHander)( NSString * error);  // 失败返回


@interface HttpRequest : NSObject

@property (nonatomic,copy) NetSuccessHander succblock;
@property (nonatomic,copy) NetFailHander errorblock;



/**
 post 请求方法

 @param params 请求参数
 @param method 方法名
 @param hander 开始请求
 @param success 请求成功
 @param failHander 请求失败
 */
+(void)requestPostWithParm:(NSDictionary *)params
                    method:(NSString *)method
               startHander:(NetStartHander)hander
                   success:(NetSuccessHander)success
                fialHander:(NetFailHander)failHander
                   needPwd:(BOOL)need;

@end
