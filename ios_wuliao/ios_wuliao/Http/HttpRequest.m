//
//  HttpRequest.m
//  BLTSZY
//
//  Created by Charlie on 2017/3/29.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "HttpRequest.h"
#import <AVFoundation/AVFoundation.h>

@interface HttpRequest ()<NSXMLParserDelegate>
{
    NSMutableString * _result;
    NSMutableString *_errorStr;
}
@property (nonatomic,copy) NSString * appStr;
@end


@implementation HttpRequest


+(void)requestPostWithParm:(NSDictionary *)params
                  method:(NSString *)method
              startHander:(NetStartHander)hander
                  success:(NetSuccessHander)success
                fialHander:(NetFailHander)failHander needPwd:(BOOL)need{
//
    NSString * urlString = baseUrl;//[NSString stringWithFormat:@"%@%@",baseUrl,pageurl];
    NSLog(@"%@",urlString);
    //参数列表
  
    NSURL *url = [NSURL URLWithString:baseUrl];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    req.HTTPMethod = @"POST";
    [req setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSString * action=[NSString stringWithFormat:@"http://app.toray.cn/%@",method];
    [req setValue:action forHTTPHeaderField:@"SOAPAction"];
    req.timeoutInterval = 60;//超时
    NSMutableString * str = [NSMutableString string];
    NSMutableDictionary * mutDic=[NSMutableDictionary dictionaryWithDictionary:params];
    NSDictionary * info = [[UserInfoCache share] userInfo];
    if (need) {
        for (NSString * key in info) {
            NSString * value =[info[key] length] ? info[key] : @"";
            [mutDic setObject:value forKey:key];
        }
    }
    for (NSString * key in mutDic) {
         
        NSString * value =[mutDic[key] length] ? mutDic[key] : @"";
        NSString * item = [NSString stringWithFormat:@"<%@>%@</%@>\\",key,value,key];
        [str appendString:item];
    }
 
    NSString * body = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\
                       <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\
                       <soap:Body>\
                       <%@ xmlns=\"http://app.toray.cn/\">\
                       %@\
                       </%@>\
                       </soap:Body>\
                       </soap:Envelope>",method,str,method];
    
    NSData *reqData = [body dataUsingEncoding:NSUTF8StringEncoding];
    req.HTTPBody = reqData;
    HttpRequest * requsetData=[HttpRequest new];
    requsetData.appStr = method;
    requsetData.succblock = success;
    requsetData.errorblock = failHander;
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (connectionError) {
            NSLog(@"Error: %@ %@",method,connectionError);
            showFadeOutView(NSLocalizedString(@"网络错误",nil), NO, 2);
            failHander(connectionError.description);
        }
        else
        {
           // NSLog(@"XML:%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            NSXMLParser * parse=[[NSXMLParser alloc]initWithData:data];
            parse.delegate = requsetData;
            if ([parse parse]) {
                NSLog(@"正在解析");
            }
        }
    }];
    
 
}
-(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *str=[[NSString alloc] initWithString:_result];
    
    if ([elementName isEqualToString:@"faultstring"]) {
        NSLog(@"faultstring=%@",str);
        if (self.errorblock) {
            self.errorblock(str);
        }
    }
    NSString * title=[NSString stringWithFormat:@"%@Result",self.appStr];
    if ([elementName isEqualToString:title]) {
        NSLog(@"%@",str);
        if (self.succblock) {
            NSDictionary * dic= [self parseJSONStringToNSDictionary:str];
            if ([dic[@"returnCode"] integerValue] == 10003){
                showFadeOutView(@"登录失败，请重新登录", NO, kDelayTime);
                UIViewController * vc =[[FYKeyBoardManger shareManger] visibleVC];
                [MBProgressHUD hideHUDForView:vc.view animated:YES];
            }else if([dic[@"returnCode"] integerValue] == 10004) {
                UIViewController * vc =[[FYKeyBoardManger shareManger] visibleVC];
                [MBProgressHUD hideHUDForView:vc.view animated:YES];
                showFadeOutView(@"密码已过期，需要重新修改！", NO, kDelayTime);
            }else{
               self.succblock(dic);
            }
        }
    }
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    //获取文本节点中的数据，因为下面的方法要保存这里获取的数据，所以要定义一个全局变量(可修改的字符串)
    //这里要赋值为空，目的是为了清空上一次的赋值
  //  [_result setString:@""];
    [_result appendString:string];//string是获取到的文本节点的值，只要是文本节点都会获取(包括换行)，然后到下个方法中进行判断区分
}
- (void)parserDidStartDocument:(NSXMLParser *)parser{
    _result=[NSMutableString string];
}
//解析结束执行的方法
- (void)parserDidEndDocument:(NSXMLParser *)parser{
   // NSLog(@"解析结束执行的方法");
}
//当出现解析错误的时候，会执行这个方法
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
     // NSLog(@"解析cuowu方法");
}
@end
