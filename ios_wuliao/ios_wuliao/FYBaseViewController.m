//
//  FYBaseViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseViewController.h"

@interface FYBaseViewController ()

@end

@implementation FYBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLORRGB(242, 242, 242);
}

-(void)setNavigationBarCLearBackground{
    [self.navigationController.navigationBar setBackgroundImage:[self cLearImageSize:CGSizeMake(kScreenWidth, 64)] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[self cLearImageSize:CGSizeMake(kScreenWidth, 1)]];
}
-(void)setNavigationBarDefault{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    
}
-(UIImage *)cLearImageSize:(CGSize)size{
    UIColor * color =[UIColor clearColor];
    CGRect rect = CGRectMake(0, 0,size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)showAlertMessage:(NSString *)message clickBlock:(dispatch_block_t)block{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:NSLocalizedString(message, nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block();
    }];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}
-(UIImageView *)checkImageViewStatus:(NSInteger)status{
    NSString * imageName=nil;
    if ([[FYBaseViewController getPreferredLanguage] hasPrefix:@"ja"]) {
        imageName = @"audited_Japan";
    } else {
        imageName = @"checked";
    }
    UIImageView * image=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
    image.frame = CGRectMake(kScreenWidth - 120, 150, 50, 50) ;
    return image;
}
+ (NSString*)getPreferredLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    return preferredLang;
}
@end
