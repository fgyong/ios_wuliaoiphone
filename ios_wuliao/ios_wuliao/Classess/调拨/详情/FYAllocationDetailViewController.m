//
//  FYAllocationDetailViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYAllocationDetailViewController.h"

@interface FYAllocationDetailViewController ()
<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSArray *_array;
    UITextField * _liyouField;
}

@end

@implementation FYAllocationDetailViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[FYKeyBoardManger shareManger] setValible:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[FYKeyBoardManger shareManger] setValible:YES];
}
-(void)doSomeThingWhenShenpi{
    if ([self.delegate respondsToSelector:@selector(shenpiSuccess)]) {
        [self.delegate performSelector:@selector(shenpiSuccess)];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setMMDBackItem:NO];
    if (self.titleStr.length) {
        [self setMMDTitle:self.titleStr];
    } else {
         [self setMMDTitle:@"调拨审核"];
    }
}
-(void)configUI{
    _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableHeaderView = nil;
    _tableView.backgroundColor = Color_DefaultBackground;
    [self.view addSubview:_tableView];
    _array = @[@"转出仓库",@"转入仓库",
               @"转出部门",@"转入部门",
               @"调拨日期",@"业务员",
               @"制单员",@"备注",];
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHeaderCell" bundle:nil] forCellReuseIdentifier:@"saleheadercell"];
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleDetailSecondCell" bundle:nil] forCellReuseIdentifier:@"FYSaleDetailSecondCell"];
    //FYSaleHejiCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHejiCell" bundle:nil] forCellReuseIdentifier:@"FYSaleHejiCell"];
    //FYShouldSaveTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYShouldSaveTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYShouldSaveTableViewCell"];
    //FYReceivedTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYReceivedTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYReceivedTableViewCell"];
    
    //FYAllocDetailCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYAllocDetailCell" bundle:nil] forCellReuseIdentifier:@"FYAllocDetailCell"];
    
    _tableView.estimatedRowHeight = 21;
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(6);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-50);
    }];

    UIView * footerView=[UIView new];
    footerView.backgroundColor =[UIColor whiteColor];
    
    UIView * lineTop=[[UIView alloc]initWithFrame:CGRectZero];
    lineTop.backgroundColor = Color_DefaultBackground;
    [footerView addSubview:lineTop];
    [lineTop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(footerView);
        make.right.mas_equalTo(footerView);
        make.top.mas_equalTo(footerView);
        make.height.mas_equalTo(6);
    }];
    
    UIButton * btn=[UIButton new];
    [btn setTitle:NSLocalizedString(@"驳回", nil) forState:UIControlStateNormal];
    btn.clipsToBounds = YES;
    
     
    btn.backgroundColor = COLORRGB(243, 101, 23);
    [footerView addSubview:btn];
    [btn addTarget:self action:@selector(notSave) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = Font(16);
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 28));
        make.top.mas_equalTo(lineTop.mas_bottom).offset(8);
        make.left.mas_equalTo(70);
    }];
    
    UIButton * btn2=[UIButton new];
    [btn2 setTitle:NSLocalizedString(@"批准", nil) forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor greenColor] forState:0];
    btn2.clipsToBounds = YES;
    btn2.backgroundColor = COLOR_Subject_Color;
    btn2.titleLabel.font = Font(16);
    [footerView addSubview:btn2];
    [btn2 addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 28));
        make.top.mas_equalTo(btn);
        make.right.mas_equalTo(footerView.mas_right).offset(-70);
    }];
    
    
    [self.view addSubview:footerView];
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
}

-(void)notSave{
    [self showShenpi:@"2" title:@"驳回" Liyou:@"驳回"];
}
-(void)showShenpi:(NSString *)statue title:(NSString *)title Liyou:(NSString*)liyou{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self pizhunString:_liyouField.text status:statue];
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = NSLocalizedString(liyou, nil);
        _liyouField = textField;
        textField.returnKeyType = UIReturnKeyDone;
    }];
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)save{
    [self showShenpi:@"1" title:@"批准" Liyou:@"批准"];
}
-(void)pizhunString:(NSString *)str status:(NSString*)code{
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (_liyouField.text.length) {
        [dic setObject:_liyouField.text forKey:@"opinion"];
    }
    [dic setObject:code forKey:@"ApproveState"];
//    [dic setObject:self.order_id forKey:@"Dlid"];
//    if ([_dicHeader[@"ccode"] length]) {
//        [dic setValue:_dicHeader[@"ccode"] forKey:@"Dlcode"];
//    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [HttpRequest requestPostWithParm:dic
                              method:@"Dispatchlist_approve"
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                              
                             NSString * message = @"";
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 message = @"审核成功";
                                 if ([code isEqualToString:@"2"]) {
                                     message = @"驳回成功";
                                 }
                             } else {
                                 message = @"审核失败，联系系统管理员";
                                 }
                             [self showAlertMessage:message clickBlock:^{
                                 [self doSomeThingWhenShenpi];
                                 [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                             }];

                           
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:return  8;break;
        case 1:return 3;break;
 
        default:
            break;
    }
    return 13;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:return 21;break;
        case 1:return 75;break;
            
        default:
            break;
    }
    return 32;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
            return 46;
            break;
            
        default:
            break;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
        {
            FYSaleHeaderView * view = [[FYSaleHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46) title:@"出货明细" ImageName:@"shippingDetail"];
            view.backgroundColor = [UIColor whiteColor];
            return view;
            
        }
            default:
            break;
    }
    return nil;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            FYSaleHeaderCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"saleheadercell" forIndexPath:indexPath];
            cell.titleLabel.text = NSLocalizedString(_array[indexPath.row],nil);
;
            cell.contentLabel.text = @"XXXXXXXXXXXXXXXXXXXX";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }break;
        case 1:{
            FYAllocDetailCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYAllocDetailCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }break;
            
    }
    return [UITableViewCell new];
}
@end
