//
//  FYAllocDetailCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYAllocDetailCell.h"

@implementation FYAllocDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDic:(NSDictionary *)info{
    self.good.text =    [self getString:info[@"cinvname"]];
    self.number.text =  [self getString:info[@"iquantity"]];
    self.xinghao.text = [self getString:info[@"cInvStd"]];
    self.gongyingshang.text =[self getString:info[@"cinvdefine1"]];//供应商
    self.danwei.text =  [self getString:info[@"cComUnitName"]];
    self.yongtu.text =  [self getString:info[@"cinvdefine2"]];
    NSString *monery=[NSString stringWithFormat:@"%@",[self getString:info[@"cbdefine2"]]];
     NSString * format= @"###,##0.00";
    NSString * allMoneryString=[NSString stringWithFormat:@"%@ CNY",[self monery:monery WithForamt:format]];
    self.lastChuKuJineLabel.text = allMoneryString;
}
-(NSString * )monery:(NSString *)monery WithForamt:(NSString *)format{
    if ([monery length] == 0) {
        monery = @"";
    }
    CGFloat Monery= [monery floatValue];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:format];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:Monery]];
    return formattedNumberString;
}
@end
