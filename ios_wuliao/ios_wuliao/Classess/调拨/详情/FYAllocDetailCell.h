//
//  FYAllocDetailCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYAllocDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *good;
@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *gongyingshang;
@property (weak, nonatomic) IBOutlet UILabel *xinghao;
@property (weak, nonatomic) IBOutlet UILabel *danwei;
@property (weak, nonatomic) IBOutlet UILabel *yongtu;
@property (weak, nonatomic) IBOutlet UILabel *bottomLine;
//预估出库金额
@property (weak, nonatomic) IBOutlet UILabel *lastChuKuJineLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastChuKuJinTextLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LayoutDistanceToBottomLine;

-(void)setDic:(NSDictionary *)info;
@end
