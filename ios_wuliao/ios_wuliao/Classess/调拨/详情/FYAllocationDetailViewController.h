//
//  FYAllocationDetailViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseViewController.h"
#import "FYSaleHeaderView.h"
#import "FYSaleHeaderCell.h"
#import "FYAllocDetailCell.h"



@protocol FYAllocationDetailViewControllerDelegate <NSObject>

@optional
-(void)shenpiSuccess;

@end

@interface FYAllocationDetailViewController : FYBaseViewController

/**
 0 是调拨
 1 是其他
 */
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,weak) id<FYAllocationDetailViewControllerDelegate> delegate;
@end
