//
//  FYAllocationViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYAllocationViewController.h"
#import "FYAllocationDetailViewController.h"
#import "FYOtherDetailViewController.h"

@interface FYAllocationViewController ()<FYAllocationDetailViewControllerDelegate,FYOtherDetailViewControllerDelegate>

@end

@implementation FYAllocationViewController
-(void)shenpiSuccess{
    [self.tableView freshHeader];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView freshHeader];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"调拨"];
   
    

    self.tableView =[[ FYBaseTableViewController alloc]initWithStyle:UITableViewStylePlain];
    self.tableView.sectionTitles = @[@"待审核调拨单",@"已审核调拨单"];
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    rect.size.height = kScreenHeight - 64 - 49 - 64;
    self.tableView.view.frame = rect;
    
    [self.view addSubview:self.tableView.view];
    
    __weak FYAllocationViewController * __weakSelfF = self;
    [self.tableView setSelectIndex:^(NSIndexPath *indexpath,NSString * order_id) {
        FYOtherDetailViewController * Detail=[[FYOtherDetailViewController alloc]init];
        Detail.hidesBottomBarWhenPushed = YES;
        Detail.type = 2;
        Detail.order_id = order_id;
        Detail.delegate = __weakSelfF;
        Detail.titleStr = @"调拨审核";
        Detail.needShenpi = !indexpath.section;
        [__weakSelfF .navigationController pushViewController:Detail animated:YES];
        Detail.hidesBottomBarWhenPushed = NO;
    }];
    
    
    self.tableView.type = 2;
    self.tableView.parmas=[NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                          @"dPreDate":@"",
                                                                          @"cDepcode":@"",
                                                                          @"cInvcode":@""}];
    self.tableView.funcNameTop     = @"queryTrunsVouch_NotApproved";
    self.tableView.funcNameHistory = @"queryTrunsVouch_All";
    self.tableView.type = 2;
    [self.tableView freshHeader];
}



@end
