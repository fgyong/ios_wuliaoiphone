//
//  FYDiaoboListCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYDiaoboListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *persion;

@property (weak, nonatomic) IBOutlet UILabel *fromCangku;
@property (weak, nonatomic) IBOutlet UILabel *toCangku;
@property (weak, nonatomic) IBOutlet UILabel *frombumen;
@property (weak, nonatomic) IBOutlet UILabel *toBumen;
@property (weak, nonatomic) IBOutlet UILabel *danhao;
-(void)seteDic:(NSDictionary *)info;
@end
