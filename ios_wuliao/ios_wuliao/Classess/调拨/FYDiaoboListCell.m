//
//  FYDiaoboListCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYDiaoboListCell.h"

@implementation FYDiaoboListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)seteDic:(NSDictionary *)info{
    self.frombumen.text = [self getString:info[@"cOutdepname"]];
    self.toBumen.text = [self getString:info[@"cIndepname"]];
    
    self.fromCangku.text = [self getString:info[@"cOutwhname"]];
    
    self.toCangku.text = [self getString:info[@"cInwhname"]];
    
    self.date.text = [self getString:info[@"ddate"]];
    
    self.persion.text = [self getString:info[@"cmaker"]];
    self.danhao.text =[self getString:info[@"ccode"]];

}

@end
