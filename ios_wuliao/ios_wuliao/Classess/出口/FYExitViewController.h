//
//  FYExitViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYSaleViewController.h"
/**
 正在使用中 2018.4.19
 */
@interface FYExitViewController : FYBaseViewController
@property (nonatomic,strong) FYBaseTableViewController * tableView;
@end
