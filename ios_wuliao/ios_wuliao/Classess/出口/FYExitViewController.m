//
//  FYExitViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYExitViewController.h"
#import "FYSalesDetailViewController.h"
#import "FYSelectViewController.h"

@interface FYExitViewController ()

@end

@implementation FYExitViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView freshHeader];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"出口"];
    [self setMMDRightItemWithTitle:@"查询" action:@selector(select)];
    self.tableView =[[ FYBaseTableViewController alloc]initWithStyle:UITableViewStylePlain];
    self.tableView.title=@"";
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    rect.size.height = kScreenHeight - 64 - 49 - 64;
    self.tableView.view.frame = rect;
    
    [self.view addSubview:self.tableView.view];
    __weak FYExitViewController *__weakSelf = self;
    [self.tableView setSelectIndex:^(NSIndexPath *indexpath,NSString * orderId) {
        NSLog(@"section:%ld row:%ld",indexpath.section,indexpath.row);
        FYSalesDetailViewController * vc=[[FYSalesDetailViewController alloc]init];
        vc.type = 1;
        vc.hidesBottomBarWhenPushed = YES;
        vc.titleStr = @"出口审核";
        vc.order_id = orderId;
        if (indexpath.section == 0) {
            vc.needShenpi = YES;
        }
        [__weakSelf.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = NO;
    }];
    self.tableView.urlType = 0;
    self.tableView.parmas=[NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                          @"dPreDate":@"",
                                                                          @"cDepcode":@"",
                                                                          @"cInvcode":@""}];
    self.tableView.sectionTitles   = @[@"待审核出口销货单",@"已审核出口销货单"];
    self.tableView.funcNameTop     = @"queryExConsignment_NotApproved";
    self.tableView.funcNameHistory = @"queryExConsignment_All";
    [self.tableView freshHeader];
}
-(void)select{
    FYSelectViewController * fy =[[FYSelectViewController alloc]init];
    [fy successBlock:^(NSDictionary *result) {
        self.tableView.parmas = [NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                                @"dPreDate":@"",
                                                                                @"cDepcode":@"",
                                                                                @"cInvcode":@""}];
        for (NSString * item in result) {
            [self.tableView.parmas setObject:result[item] forKey:item];
        }
        [self.tableView freshHeader];
    }];
    fy.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fy animated:YES];
    fy.hidesBottomBarWhenPushed = NO;
}

@end
