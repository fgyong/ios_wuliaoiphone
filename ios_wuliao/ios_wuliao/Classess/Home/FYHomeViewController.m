//
//  FYHomeViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYHomeViewController.h"
#import "FYSalesDetailViewController.h"
#import "FYOtherDetailViewController.h"
#import "FYAllocationDetailViewController.h"
#import "FYSelectViewController.h"


@interface FYHomeViewController ()<UITableViewDelegate,UITableViewDataSource,FYOtherDetailViewControllerDelegate,FYSalesDetailViewControllerDelegate>
{
    NSArray *_sectionImageNames;
    NSArray *_sectionTitles;
    NSMutableArray *_openStatus;
    NSInteger closeTag;
    NSMutableArray * _dataSource;
    
    UITextField * _oldTextfield;
    UITextField *_newTextfield;
}

@property (weak, nonatomic) IBOutlet UILabel *NameLable;
@end

@implementation FYHomeViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setNavigationBarCLearBackground];
    NSString * name = [[UserInfoCache share]name];
    [self setMMDTitle:name fontSize:[UIFont systemFontOfSize:24]withBackgroundColor:nil titleColor:nil];
    [self getDataFromServer];
    
 
}
 
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
 
 
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
 
    [self setNavigationBarDefault];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBarCLearBackground];
    //[self setMMDItemTitle:@"历史查询" action:@selector(select) isLeftItem:NO withtextcolor:[UIColor whiteColor]];
    NSString * name = [[UserInfoCache share]name];
    [self setMMDTitle:name fontSize:[UIFont systemFontOfSize:24]withBackgroundColor:nil titleColor:nil];
    //*********************************
    //***********88历史单据按钮***********
    [self.historyBtn setClipsToBounds:YES];
    self.historyBtn.layer.cornerRadius = 4;
    [self.historyBtn setTitleColor:COLOR_Subject_Color forState:0];
    [self.historyBtn setTitle:NSLocalizedString(@"历史单据查询", nil) forState:0];
    [self.historyBtn addTarget:self action:@selector(select) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.tableView =[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 30;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(13);
        make.right.mas_equalTo(self.view.mas_right).offset(-13);
        make.bottom.mas_equalTo(self.logOutBtn.mas_top).offset(-13);
        make.top.mas_equalTo(self.historyBtn.mas_bottom).offset(8);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FYHomeCell" bundle:nil] forCellReuseIdentifier:@"homeCell"];
    [self.tableView registerClass:[FYHeaderView class] forHeaderFooterViewReuseIdentifier:@"view"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _sectionTitles = @[NSLocalizedString(@"销售发货单", nil),NSLocalizedString(@"出口销货单", nil),NSLocalizedString(@"其他出库单", nil),NSLocalizedString(@"调拨单", nil)];
    _sectionImageNames = @[@"sales",@"export",@"other",@"transfer"];
    _openStatus =[NSMutableArray arrayWithArray: @[@0,@0,@0,@0]];
    
    closeTag = 5;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(getDataFromServer)];
    
    _dataSource =[NSMutableArray array];
    [_dataSource addObject:@[]];
    [_dataSource addObject:@[]];
    [_dataSource addObject:@[]];
    [_dataSource addObject:@[]];
}
-(void)select{
    FYSelectSecondVC * Vc=[[FYSelectSecondVC alloc]init];
    UINavigationController * na=[[UINavigationController alloc]initWithRootViewController:Vc];
    Vc.funcName = @"queryApprovedInfo";
    Vc.type = 5;
    [self.navigationController presentViewController:na animated:YES completion:nil];

}
-(void)getDataFromServer{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpRequest requestPostWithParm:@{}
                              method:@"queryPendingApprovalInfo"
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES ];
        [self.tableView.mj_header endRefreshing];
        if ([result[@"returnCode"] integerValue] == 10000) {
            NSLog(@"%@",result.description);
            NSArray * array =  result[@"aryList"];
                [self handleSource:array];
        } else {
            showFadeOutView(result[@"errDesc"], NO, 2);
        }
    } fialHander:^(NSString *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES ];
    } needPwd:YES];

}
-(void)handleSource:(NSArray *)list{
    NSMutableArray * array0=[NSMutableArray array];
    NSMutableArray * array1=[NSMutableArray array];
    NSMutableArray * array2=[NSMutableArray array];
    NSMutableArray * array3=[NSMutableArray array];
    NSInteger count = list.count;
    for (NSInteger i = 0; i < count; i ++) {
        NSDictionary * info = list[i];
        NSString * type = info[@"cType"];
        if ([type isEqualToString:@"01"] ) {
            [array0 addObject:info];
        } else      if ([type isEqualToString:@"EX24"] ) {
            [array1 addObject:info];
        } else     if ([type isEqualToString:@"0302"] ) {
            [array2 addObject:info];
        } else     if ([type isEqualToString:@"0304"] ) {
            [array3 addObject:info];
        }
    }
    [_dataSource insertObject:[array0 copy] atIndex:0];
    [_dataSource insertObject:[array1 copy] atIndex:1];
    [_dataSource insertObject:[array2 copy] atIndex:2];
    [_dataSource insertObject:[array3 copy] atIndex:3];
    [_tableView reloadData];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    FYHeaderView * view =[[FYHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
    
    BOOL open = [_openStatus[section] integerValue]  == 0 ? YES : NO;
    NSString *number = @"0";//[NSString stringWithFormat:@"%ld", [_dataSource[section] count]];
    NSArray * item = _dataSource[section];
    if ([item isKindOfClass:[NSArray class]]) {
        if (item.count > 0) {
            number=[NSString stringWithFormat:@"%ld",item.count];
        }
    }
    [view title:_sectionTitles[section] imageName:_sectionImageNames[section] number:number openStatus:open];
     
    __weak FYHomeViewController * __weakSelf = self;
    [view setClickBlock:^(NSInteger number){
        _openStatus[number] = ([_openStatus[number] integerValue] == 0) ? @1 : @0;
        [__weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:number] withRowAnimation:UITableViewRowAnimationNone];
    }];
    view.tag = section;
    return view;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([_openStatus[section] boolValue] == YES) {
        return [_dataSource[section] count];
    }
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    return 29;
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FYHomeCell * cell = [tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    NSDictionary * Info = _dataSource[indexPath.section][indexPath.row];
    NSString * Message =[Info objectForKey:@"cMessage"];
    
//    cell.timeLabel.text = [NSString stringWithFormat:@"12:00"];
//    NSString * tijiao=[NSString stringWithFormat:@"%@",NSLocalizedString(@"提交的", nil)];
//    NSString * fahuodan=[NSString stringWithFormat:@"%@",NSLocalizedString(@"发货单", nil)];
//    NSString * shenpi=[NSString stringWithFormat:@"%@",NSLocalizedString(@"需要你审批", nil)];
//    NSString * strAll=[NSString stringWithFormat:@"XXX%@%@XXX%@",tijiao,fahuodan,shenpi];
//    NSMutableAttributedString * str = [[NSMutableAttributedString alloc]initWithString:strAll];
//    [str addAttribute:NSForegroundColorAttributeName value:COLOR_Subject_Color range:NSMakeRange(0, 3)];
    
    NSArray * array = [Message componentsSeparatedByString:@" "];
    NSArray * tijiaoArray =@[@"提交的销售发货单",
                       @"提交的出口销货单",
                       @"提交的其他出库单",
                       @"提交的调拨单"];
    NSString * all=[NSString stringWithFormat:@"%@ %@ %@ %@",array[0],NSLocalizedString(tijiaoArray[indexPath.section],nil),array[2],NSLocalizedString(@"已进入审批流，等待您的审批！", nil)];
    cell.titleLabel.text = all;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * order =_dataSource[indexPath.section][indexPath.row] [@"cID"];
    switch (indexPath.section) {
        case 0:[self pushSaleWithInfo:order];break;
        case 1:[self pushExitWithInfo:order];break;
        case 2:[self pushOtherWithDIc:order];break;
        case 3:[self pushAllocation:order];break;
        default:
            break;
    }
    NSLog(@"cell clicked hahah");

}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
     NSLog(@"cell clicked ha2222hah");
}
-(void)pushSaleWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc =[[FYSalesDetailViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.order_id = orderID;
    vc.delegate = self;
    vc.needShenpi = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushOtherWithDIc:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.order_id = orderid;
    vc.needShenpi = YES;
    vc.delegate = self;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushAllocation:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.type = 2;
    vc.delegate = self;
    vc.needShenpi = YES;
    vc.order_id = orderid;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushExitWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc=[[FYSalesDetailViewController alloc]init];
    vc.type = 1;
    vc.delegate = self;
    vc.hidesBottomBarWhenPushed = YES;
    vc.titleStr = @"出口审核";
    vc.order_id = orderID;
    vc.needShenpi = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)shenpiSuccess{
    [self getDataFromServer];
}
-(void)shenpiStatusComplate {
    [self getDataFromServer];
}
- (IBAction)logOutClick:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"退出登录", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UserInfoCache share] clearCache];//清除之前的缓存
        AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        [appdelegate resetTags];
        [appdelegate changeLogin];
    }];
    
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
//    AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
//    [appdelegate changeLogin];
}
@end
