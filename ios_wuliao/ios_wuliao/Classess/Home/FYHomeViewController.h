//
//  FYHomeViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYHomeCell.h"
#import "JPUSHService.h"



@interface FYHomeViewController : FYBaseViewController

@property ( nonatomic,strong)  UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *homeBg;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;

- (IBAction)logOutClick:(id)sender;



@property (weak, nonatomic) IBOutlet UIButton *historyBtn;




@end
