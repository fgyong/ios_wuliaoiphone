//
//  FYSelectCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSelectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@end
