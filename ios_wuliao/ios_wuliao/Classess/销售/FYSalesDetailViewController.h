//
//  FYSalesDetailViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/1.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseViewController.h"
#import "FYSaleDetailSecondCell.h"
#import "FYSaleHeaderCell.h"
#import "FYSaleHeaderView.h"
#import "FYSaleHejiCell.h"
#import "FYShouldSaveTableViewCell.h"
#import "FYReceivedTableViewCell.h"
@protocol FYSalesDetailViewControllerDelegate <NSObject>

@optional
-(void)shenpiSuccess;

@end

@interface FYSalesDetailViewController : FYBaseViewController
@property (nonatomic,copy) NSString * order_id;
@property (nonatomic,weak) id<FYSalesDetailViewControllerDelegate> delegate;
@property (nonatomic,assign) BOOL needShenpi;

/**
 // 0 是销售 
 // 1 是出口
 */
@property (nonatomic,assign) NSInteger type;
@end
