//
//  FYBaseTableViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseTableViewController.h"

@interface FYBaseTableViewController (){
   
    NSArray *_sectionTitles;
    NSMutableArray *_openStatus;
    NSInteger closeTag;

    NSInteger _page ;
}

@end

@implementation FYBaseTableViewController

-(void)setSectionTitles:(NSArray *)sectionTitles{
    NSMutableArray * array=[NSMutableArray array];
    for (int i = 0; i < sectionTitles.count; i ++) {
        [array addObject:NSLocalizedString(sectionTitles[i], nil)];
    }
    _sectionTitles = [array copy];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"主页" withBackgroundColor:nil titleColor:[UIColor whiteColor]];
    if (_sectionTitles == nil) {
            _sectionTitles = @[NSLocalizedString(@"待审核发货单", nil),NSLocalizedString(@"已审核发货单", nil)];
    }
    _page = 0;
    _openStatus =[NSMutableArray arrayWithArray: @[@1,@0]];
    closeTag = 3;
    self.dataSource = [NSMutableArray array];
    self.dataHistorySource=[NSMutableArray array];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FYBaseCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYBaseCellTableViewCell"];
    [self.tableView registerClass:[FYBaseTableViewHeaderView class] forHeaderFooterViewReuseIdentifier:@"FYBaseTableViewHeaderView"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FYOtherCell" bundle:nil] forCellReuseIdentifier:@"FYOtherCell"];
    //2 FYDiaoboListCell
    [self.tableView registerNib:[UINib nibWithNibName:@"FYDiaoboListCell" bundle:nil] forCellReuseIdentifier:@"FYDiaoboListCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(freshHeader)];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshFooter)];
    [self freshHeader];
}
-(void)refreshFooter{
    _page ++;
    [_parmas setValue:[NSString stringWithFormat:@"%ld",_page] forKey:@"pageindex"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpRequest requestPostWithParm:_parmas
                              method:self.funcNameHistory
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if ([result[@"returnCode"] integerValue] == 10000) {
           // NSLog(@"%@",result.description);
            NSArray * array =  result[@"aryList"];
            if (_page == 1) {
                [_dataHistorySource removeAllObjects];
            }
            if (array.count) {
                [_dataHistorySource addObjectsFromArray:result[@"aryList"]];
                //_openStatus[1]= @(1);
                [self.tableView reloadData];
            }
        } else {
            showFadeOutView(result[@"errDesc"], NO, 2);
        }
    } fialHander:^(NSString *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } needPwd:YES];
}
/**
 头部刷新
 */
-(void)freshHeader{
    [_dataHistorySource removeAllObjects];
    [self.tableView reloadData];
    [self getDatafromServerHeader];
    
    [self refreshFooter];
}
-(void)getDatafromServerHeader{
 
    _page = 0;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpRequest requestPostWithParm:_parmas
                              method:self.funcNameTop
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if ([result[@"returnCode"] integerValue] == 10000) {
            //NSLog(@"%@",result.description);
            NSArray * array =  result[@"aryList"];
            _openStatus[0] = @(1);
             [_dataSource removeAllObjects];
            if (array.count) {
                [_dataSource addObjectsFromArray:result[@"aryList"]];
                [self.tableView reloadData];
            }
        } else {
            showFadeOutView(result[@"errDesc"], NO, 2);
        }
    } fialHander:^(NSString *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } needPwd:YES];
}
#pragma mark - Table view data source
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    FYBaseTableViewHeaderView * view = (FYBaseTableViewHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FYBaseTableViewHeaderView"];
    BOOL open = [_openStatus[section] integerValue]  == 0 ? YES : NO;
    [view title:_sectionTitles[section] open:open];
    NSInteger count = _dataSource.count;
    if (section == 0) {
         view.NumberLabel.text =[NSString stringWithFormat:@"%ld",count];
        view.NumberLabel.hidden = NO;
    }else{
        view.NumberLabel.hidden = YES;
    }
   
    __weak FYBaseTableViewController * __weakSelf = self;
    [view setClickBlock:^(NSInteger number){
        _openStatus[number] = ([_openStatus[number] integerValue] == 0) ? @1 : @0;
        [__weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:number] withRowAnimation:UITableViewRowAnimationNone];
    }];
    view.tag = section;
    
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
 
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.type) {
        case 0:
            return 177;
            break;
        case 1: return 144;break;
        case 2:return 107;break;
        default:
            break;
    }
    return   140;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([_openStatus[section] boolValue] == YES) {
        if (section == 0) {
            return self.dataSource.count;
        }else {
          return self.dataHistorySource.count;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.type) {
        case 0:
        {
            FYBaseCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FYBaseCellTableViewCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.section == 0) {
                [cell configDIc:_dataSource[indexPath.row]];
            } else{
                [cell configDIc:_dataHistorySource[indexPath.row]];
            }
            return cell;
        }
            break;
        case 1:{
            FYOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FYOtherCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.section == 0) {
                [cell setDic:_dataSource[indexPath.row]];
            } else{
                [cell setDic:_dataHistorySource[indexPath.row]];
            }
            return cell;
        }break;
        case 2:{
            FYDiaoboListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FYDiaoboListCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.section == 0) {
                [cell seteDic:_dataSource[indexPath.row]];
            } else{
                [cell seteDic:_dataHistorySource[indexPath.row]];
            }
            return cell;
        }break;
            

        default:
            break;
    }
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FYBaseCellTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * orderID = nil;
    if (indexPath.section == 0) {
		if (_dataSource.count > indexPath.row) {
			orderID = _dataSource[indexPath.row] [@"id"];
		}
        
    }else {
		if (_dataHistorySource.count > indexPath.row) {
        	orderID = _dataHistorySource[indexPath.row] [@"id"];
		}
    }
    if (self.block) {
        self.block(indexPath,orderID);
    }
}
-(void)setSelectIndex:(clickIndexPath)block{
    self.block = block;
}

@end
