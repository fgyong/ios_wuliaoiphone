//
//  FYBaseTableViewHeaderView.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYBaseTableViewHeaderView : UITableViewHeaderFooterView
@property (assign) BOOL open;
@property (nonatomic,strong) FYLable * titleLabel;
@property (nonatomic,strong) FYLable *NumberLabel;
-(void)title:(NSString *)title
        open:(BOOL)isOpen;


-(void)setClickBlock:(clickBlock)block;

-(void)setOpenStatus:(BOOL)isOpen;
@end
