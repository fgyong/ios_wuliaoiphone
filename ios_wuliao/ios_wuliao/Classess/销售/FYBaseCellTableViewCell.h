//
//  FYBaseCellTableViewCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYBaseCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *danHao;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bumenLabel;
@property (weak, nonatomic) IBOutlet UILabel *yewuYuanLabel;
@property (weak, nonatomic) IBOutlet UILabel *chukuriLabel;
@property (weak, nonatomic) IBOutlet UILabel *cangku;
@property (weak, nonatomic) IBOutlet UILabel *jineLabel;
@property (weak, nonatomic) IBOutlet UILabel *tiaojianLabel;




-(void)configDIc:(NSDictionary *)info;
@end
