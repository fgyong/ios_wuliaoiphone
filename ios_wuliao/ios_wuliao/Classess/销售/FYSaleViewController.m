//
//  FYSaleViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSaleViewController.h"
#import "FYSelectViewController.h"
#import "FYSalesDetailViewController.h"

@interface FYSaleViewController ()<FYSalesDetailViewControllerDelegate>

@end

@implementation FYSaleViewController
-(void)shenpiSuccess{
    [self.tableView freshHeader];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView freshHeader];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"销售"];
    [self setMMDRightItemWithTitle:@"查询" action:@selector(select)];
    self.tableView =[[ FYBaseTableViewController alloc]initWithStyle:UITableViewStylePlain];
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    rect.size.height = kScreenHeight - 64 - 49 - 64;
    self.tableView.view.frame = rect;//CGRectMake(0, 0, kScreenWidth, kScreenHeight - 64 - 44);
    
    [self.view addSubview:self.tableView.view];
    __weak FYSaleViewController *__weakSelf = self;
    [self.tableView setSelectIndex:^(NSIndexPath *indexpath,NSString * orderId) {
        NSLog(@"section:%ld row:%ld",indexpath.section,indexpath.row);
        FYSalesDetailViewController * vc=[[FYSalesDetailViewController alloc]init];
        vc.hidesBottomBarWhenPushed = YES;
        vc.delegate = __weakSelf;
        vc.order_id = orderId;
        if (indexpath.section == 0) {
            vc.needShenpi = YES;
        }
        [__weakSelf.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = NO;
    }];
    self.tableView.urlType = 0;
    self.tableView.parmas=[NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                          @"dPreDate":@"",
                                                                          @"cDepcode":@"",
                                                                          @"cInvcode":@""}];
    self.tableView.funcNameTop = @"queryDispatchlist_NotApproved";
    self.tableView.funcNameHistory = @"queryDispatchlist_All";
    [self.tableView freshHeader];
}
-(void)select{
    FYSelectViewController * fy =[[FYSelectViewController alloc]init];
    [fy successBlock:^(NSDictionary *result) {
        self.tableView.parmas = [NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                                @"dPreDate":@"",
                                                                                @"cDepcode":@"",
                                                                                @"cInvcode":@""}];
        for (NSString * item in result) {
            [self.tableView.parmas setObject:result[item] forKey:item];
        }
        [self.tableView freshHeader];
    }];
    fy.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fy animated:YES];
    fy.hidesBottomBarWhenPushed = NO;
}


@end
