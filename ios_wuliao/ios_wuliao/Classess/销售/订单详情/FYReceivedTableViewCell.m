//
//  FYReceivedTableViewCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYReceivedTableViewCell.h"

@implementation FYReceivedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configDIc:(NSDictionary *)info{
    self.yewuYuan.text =[self getString:info[@"cpersonname"]] ;//[1]	(null)	@"cpersonname" : @"叶毅琪"	
    self.riqi.text = [[self getString:info[@"dVouchDate"]] componentsSeparatedByString:@" "] [0];
    self.bianHao.text =[self getString: info[@"cVouchID"]];
    self.leixing.text =[self getString: info[@"cTypeName"]];
    self.yue.text = [self getString:info[@"preiCAmount"]];
}
@end
