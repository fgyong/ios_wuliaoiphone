//
//  FYSaleDetailSecondCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSaleDetailSecondCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
//出货金额
@property (weak, nonatomic) IBOutlet UILabel *yuanbiMoneryLabel;
@property (weak, nonatomic) IBOutlet UILabel *benbiMoneryLabel;
//单价
@property (weak, nonatomic) IBOutlet UILabel *danjiaLabel;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *xinghaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *danweiLabel;
@property (weak, nonatomic) IBOutlet UILabel *shuliangLabel;
-(void)configDic:(NSDictionary *)info;
@end
