//
//  FYSaleHeaderView.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSaleHeaderView.h"

@interface FYSaleHeaderView (){
    FYLable * _lable;
    UIImageView *_imageV;
}

@end


@implementation FYSaleHeaderView

-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title ImageName:(NSString *)name{
    self = [super initWithFrame:frame];
    if (self) {
        UIView * view=[[UIView alloc]initWithFrame:frame];
        frame.size.height = 6;
        view.frame =frame;
        view.backgroundColor = Color_DefaultBackground;
        [self addSubview:view];
        _imageV =[[UIImageView alloc]initWithFrame:CGRectMake(13, 16, 19, 22)];
        _imageV.image =[ UIImage imageNamed:name];
        [self addSubview:_imageV];
        
        _lable=[[FYLable alloc]initWithFrame:CGRectMake(_imageV.mj_size.width+_imageV.frame.origin.x + 12, 21, 220, 16) font:Font(14) bgColor:nil text:NSLocalizedString(title, nil) textColor:COLOR_Subject_Color];
        [self addSubview:_lable];
        self.backgroundColor =[UIColor whiteColor];
    }
    return self;
}
@end
