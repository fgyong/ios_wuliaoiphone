//
//  FYSaleHeaderView.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSaleHeaderView : UIView
-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title ImageName:(NSString *)name;
@end
