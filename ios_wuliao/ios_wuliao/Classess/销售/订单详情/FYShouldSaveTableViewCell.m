//
//  FYShouldSaveTableViewCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYShouldSaveTableViewCell.h"

@implementation FYShouldSaveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configDic:(NSDictionary *)info{
    self.zero_ten.text   = [self getString:info[@"zero_ten_iDAmount"] ];
    self.ten_two.text    = [self getString:info[@"eleven_twenty_iDAmount"]];
    self.two_thist.text  = [self getString:info[@"twentyone_thirty_iDAmount"] ];
    self.thisrt_six.text = [self getString:info[@"thirtyone_sixty_iDAmount"]];
    self.six_night.text  = [self getString:info[@"sixtyone_ninty_iDAmount"] ];
    self.night_later.text= [self getString:info[@"above_ninty_iDAmount"]];
    self.shouReLabel.text= [self getString:info[@"shouldRevBalance"]];//[6]	(null)	@"shouldRevBalance" : @"1,887,702.30"
    self.daoqixiaoji.text= [self getString: info[@"subiSum"]];
    self.weidaoqi.text   = [self getString: info[@"iDAmountUnexpired"]];
}
-(NSString *)allNoNnWith:(NSDictionary * )info{
    CGFloat count = [info[@"zero_ten_iDAmount"] floatValue];
    count += [info[@"eleven_twenty_iDAmount"] floatValue];
    count += [info[@"twentyone_thirty_iDAmount"] floatValue];
    count += [info[@"thirtyone_sixty_iDAmount"] floatValue];
    count += [info[@"sixtyone_ninty_iDAmount"] floatValue];
    count += [info[@"above_ninty_iDAmount"] floatValue];
    return [NSString stringWithFormat:@"%.2f",count];
}
-(NSString *)benBi:(NSDictionary *)info{
    CGFloat count = [info[@"zero_ten_iDAmount"] floatValue];
    count += [info[@"eleven_twenty_iDAmount"] floatValue];
    count += [info[@"twentyone_thirty_iDAmount"] floatValue];
    count += [info[@"thirtyone_sixty_iDAmount"] floatValue];
    count += [info[@"sixtyone_ninty_iDAmount"] floatValue];
    count += [info[@"above_ninty_iDAmount"] floatValue];
    count += [info[@"iDAmountUnexpired"] floatValue];
    return [NSString stringWithFormat:@"%.2f",count];
}
@end
