//
//  FYSaleDetailSecondCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSaleDetailSecondCell.h"

@implementation FYSaleDetailSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configDic:(NSDictionary *)info{
    self.adressLabel.text = info[@"cwhname"];
    self.xinghaoLabel.text = info[@"cInvStd"];
    self.danweiLabel.text = info[@"cComUnitName"];
    self.yuanbiMoneryLabel.text= info[@"cexch_name"];
    self.benbiMoneryLabel.text = @"暂无";
    self.name.text = info[@"cinvname"];
    self.shuliangLabel.text = info[@"iquantity"];
	//单价

}
@end
