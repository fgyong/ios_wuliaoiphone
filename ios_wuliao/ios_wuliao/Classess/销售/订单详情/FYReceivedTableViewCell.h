//
//  FYReceivedTableViewCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYReceivedTableViewCell : UITableViewCell
-(void)configDIc:(NSDictionary *)info;
@property (weak, nonatomic) IBOutlet UILabel *yewuYuan;
@property (weak, nonatomic) IBOutlet UILabel *riqi;
@property (weak, nonatomic) IBOutlet UILabel *bianHao;
@property (weak, nonatomic) IBOutlet UILabel *leixing;
@property (weak, nonatomic) IBOutlet UILabel *yue;

@end
