//
//  FYBaseTableViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//
#import "FYBaseTableViewHeaderView.h"
#import <UIKit/UIKit.h>
#import "FYBaseCellTableViewCell.h"
#import "FYOtherCell.h"
#import "FYAllocDetailCell.h"
#import "FYDiaoboListCell.h"

@protocol FYBaseTableViewController  <NSObject>

@optional
- (UITableViewCell *)fy_tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end


typedef void(^clickIndexPath)(NSIndexPath * indexpath,NSString * orderid);
@interface FYBaseTableViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray  * dataHistorySource;
@property (nonatomic,strong) NSMutableArray  * dataSource;
//sectionTitles
@property (nonatomic,strong) NSArray * sectionTitles;

/**
 1 是其他
 2 是调拨
 */
@property (nonatomic,assign) NSInteger type;

@property (nonatomic,copy) clickIndexPath block;

/**
 0 销售
 1 出口
 2 其他
 3 调拨
 */
@property (nonatomic,assign) NSInteger urlType;//每个种类都有自己的种类

/**
 url 请求的参数
 */
@property (nonatomic,strong) NSMutableDictionary * parmas;

/**
 请求方法名字
 */
@property (nonatomic,copy) NSString * funcNameTop;
/**
 请求方法名字
 */
@property (nonatomic,copy) NSString * funcNameHistory;
@property (nonatomic,assign) id<FYBaseTableViewController> delgegate;
-(void)setSelectIndex:(clickIndexPath)block;
-(void)freshHeader;
@end
