//
//  FYSelectViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSelectViewController.h"
#import "FYPickerView.h"
#import "FYDatePickerView.h"
@interface FYSelectViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSArray *_titlesArray;
    FYPickerView *_picker;
    FYDatePickerView *_datePicker;
    
    
    NSMutableDictionary *_mutDic;
    NSString * _date;
    
    
    
    NSMutableArray *_dataSourceArray;
}
@end

@implementation FYSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"条件筛选"];
    [self setMMDBackItem:NO];
   
    _titlesArray = @[ NSLocalizedString(@"客户", nil),NSLocalizedString(@"预计出货日", nil),NSLocalizedString(@"存货", nil),NSLocalizedString(@"部门", nil)];
    _dataSourceArray=[NSMutableArray arrayWithObjects:@"XXX",@"XXX",@"XXX",@"XXX", nil];
    [self initTableView];
}
-(void)initTableView{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 10, kScreenWidth, kScreenHeight )];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView .separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerNib:[UINib nibWithNibName:@"FYSelectCell" bundle:nil] forCellReuseIdentifier:@"SelectCell"];
    _tableView.tableHeaderView = nil;
    
    _mutDic=[NSMutableDictionary dictionary];
    
    
    _picker=[[FYPickerView alloc]init];
    [_picker selectSuccess:^(NSInteger row) {
        NSLog(@"%ld",row);
    }];
    
    _datePicker=[[FYDatePickerView alloc]init];
    _datePicker.pickerView.datePickerMode = UIDatePickerModeDate;
    [_datePicker selectSuccess:^(NSString *dateString) {
        self->_date =[NSString stringWithString:dateString];
        [self->_mutDic setObject:dateString forKey:@"dPreDate"];
        [self->_dataSourceArray replaceObjectAtIndex:1 withObject:dateString];
        [self->_tableView reloadData];
    }];
}
-(void)successBlock:(NetSuccessHander)block{
    if (block) {
        self.success = block;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titlesArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 80;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 80)];
    UIButton * left=[UIButton new];
    left.layer.borderWidth = 1;
    left.layer.borderColor = COLOR_Subject_Color.CGColor;
    left.frame = CGRectMake(kScreenWidth/2 - 27 - 70, 44, 70, 30);
    [left setTitle:NSLocalizedString(@"重置", nil) forState:0];
    left.backgroundColor=[UIColor whiteColor];
    [left setTitleColor:COLOR_Subject_Color forState:0];
    [view addSubview:left];
    [left addTarget:self action:@selector(resetSelect) forControlEvents:UIControlEventTouchUpInside];
    
    
 
    UIButton * right=[UIButton new];
    
    right.frame = CGRectMake(kScreenWidth/2 + 7 , 44, 70, 30);
    [right setTitle:NSLocalizedString(@"查询", nil) forState:0];
    right.backgroundColor=COLOR_Subject_Color;
    [right setTitleColor:[UIColor whiteColor] forState:0];
    [view addSubview:right];
    [right addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    return view;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FYSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
    cell.titleLabel.text = _titlesArray[indexPath.row];
    cell.contentLabel.text = _dataSourceArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.row == 1) {
         [_datePicker show];
    } else {
#pragma mark -选择条件页面
         [self pushSelectWithType:indexPath.row];
    }
}
-(void)resetSelect{
    [_mutDic removeAllObjects];
    _dataSourceArray=[NSMutableArray arrayWithObjects:@"XXX",@"XXX",@"XXX",@"XXX", nil];
    [_tableView reloadData];
}
#pragma mark -点击查询按钮
-(void)save{
    if (_success) {
        self.success(_mutDic);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)pushSelectWithType:(NSInteger)type{
    NSString * funcName = @"";
    switch (type) {
            case 0:funcName = @"queryCustomerinfo";break;
            case 2:funcName = @"queryInventoryinfo";break;
             case 3:funcName = @"queryDepartmentinfo";break;
        default:
            break;
    }
    FYSelectSecondVC * Vc=[[FYSelectSecondVC alloc]init];
    UINavigationController * na=[[UINavigationController alloc]initWithRootViewController:Vc];
    Vc.funcName = funcName;
    Vc.type = type;
    [Vc setselectSeccess:^(NSDictionary *result) {
        [_dataSourceArray replaceObjectAtIndex:type withObject:result[@"cName"]];
        if (type == 3) {
            //cDepcode
            [_mutDic setObject:result[@"cCode"] forKey:@"cDepcode"];
        }else if (type == 0){
            //cCusCode
            [_mutDic setObject:result[@"cCode"] forKey:@"cCusCode"];
        }else if (type == 2){
            //cInvcode
            [_mutDic setObject:result[@"cCode"] forKey:@"cInvcode"];
        }
        [_tableView reloadData];
    }];
    [self.navigationController presentViewController:na animated:YES completion:nil];
}

@end
