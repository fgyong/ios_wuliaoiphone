//
//  FYSalesDetailViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/1.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSalesDetailViewController.h"

@interface FYSalesDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSArray *_array;
    
    NSDictionary * _dicHeader;
    NSArray *_chuhuoDetail;
    NSArray *_yushouDic;
    NSDictionary *_yingShouDic;
    
    UITextField *_liyouField;
    
    UIView *_footerView;
}
@end

@implementation FYSalesDetailViewController
-(void)doSomeThingWhenShenpi{
    if ([self.delegate respondsToSelector:@selector(shenpiSuccess)]) {
        [self.delegate performSelector:@selector(shenpiSuccess)];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[FYKeyBoardManger shareManger] setValible:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[FYKeyBoardManger shareManger] setValible:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableView];
    //    if (self.titleStr.length) {
    //        [self setMMDTitle:self.titleStr];
    //    } else {
    //        [self setMMDTitle:@"审核"];
    //    }
    if (self.needShenpi == NO){
        [self setMMDTitle:@"已审核"];
    } else {
        [self setMMDTitle:@"未审核"];
    }
    [self setMMDBackItem:NO];
    [self getDataFromServer];
}
-(void)initTableView{
    _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableHeaderView = nil;
    _tableView.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:_tableView];
    _array = @[@"客户",@"预计出货日",
               @"出库金额",
               @"结算条件",@"部门",
               @"业务员",@"制单人",
               @"信用额度",@"实时应收账款",
               @"预收金额",@"信用余额",
               @"备注"];
    
    if (self.needShenpi == NO) {
        _array = @[@"客户",@"预计出货日",
                   @"出库金额",
                   @"结算条件",@"部门",
                   @"业务员",@"制单人",
                   @"信用额度",@"备注"];
    }
    
    
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHeaderCell" bundle:nil] forCellReuseIdentifier:@"saleheadercell"];
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleDetailSecondCell" bundle:nil] forCellReuseIdentifier:@"FYSaleDetailSecondCell"];
    //FYSaleHejiCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHejiCell" bundle:nil] forCellReuseIdentifier:@"FYSaleHejiCell"];
    //FYShouldSaveTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYShouldSaveTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYShouldSaveTableViewCell"];
    //FYReceivedTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYReceivedTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYReceivedTableViewCell"];
    
    
    _tableView.estimatedRowHeight = 21;
    CGFloat bottom =  -50;
//    if (self.needShenpi == NO) {//历史记录不用审批
//        bottom = 0;
//    }
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(bottom);
    }];
    
    UIView * footerView=[UIView new];
    footerView.backgroundColor =[UIColor whiteColor];
    _footerView = footerView;
    UIView * lineTop=[[UIView alloc]initWithFrame:CGRectZero];
    lineTop.backgroundColor = Color_DefaultBackground;
    [footerView addSubview:lineTop];
    [lineTop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(footerView);
        make.right.mas_equalTo(footerView);
        make.top.mas_equalTo(footerView);
        make.height.mas_equalTo(6);
    }];
    CGFloat bottomfooter =  50;
    [self.view addSubview:footerView];
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(bottomfooter);
    }];
    if (self.needShenpi == NO) {//历史记录不用审批
        [_tableView addSubview:[self checkImageViewStatus:1]];
        UIButton * btn=[UIButton new];
        [btn setTitle:NSLocalizedString(@"弃审", nil) forState:UIControlStateNormal];
        
        btn.clipsToBounds = YES;
        [footerView addSubview:btn];
        btn.backgroundColor = COLORRGB(243, 101, 23);
        [btn addTarget:self action:@selector(notSave2) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = Font(16);
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(120, 28));
            make.top.mas_equalTo(lineTop.mas_bottom).offset(8);
            make.centerX.mas_equalTo(footerView.mas_centerX);
        }];

        return;
    }
    UIButton * btn=[UIButton new];
    [btn setTitle:NSLocalizedString(@"驳回", nil) forState:UIControlStateNormal];
    
    btn.clipsToBounds = YES;
    [footerView addSubview:btn];
    btn.backgroundColor = COLORRGB(243, 101, 23);
    [btn addTarget:self action:@selector(notSave) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = Font(16);
    
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 28));
        make.top.mas_equalTo(lineTop.mas_bottom).offset(8);
        make.right.mas_equalTo(footerView.mas_centerX).offset(-20);
    }];
    
    UIButton * btn2=[UIButton new];
    [btn2 setTitle:NSLocalizedString(@"批准", nil) forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor whiteColor] forState:0];
    btn2.clipsToBounds = YES;
    btn2.backgroundColor = COLOR_Subject_Color;
    btn2.titleLabel.font = Font(16);
    [footerView addSubview:btn2];
    [btn2 addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 28));
        make.top.mas_equalTo(btn);
        make.right.mas_equalTo(footerView.mas_right).offset(-70);
    }];
    
    
    

    
    
}
-(void)getDataFromServer{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (self.order_id) {
        if (self.type == 1) {
            [dic setObject:self.order_id forKey:@"id"];
        } else {
            [dic setObject:self.order_id forKey:@"Dlid"];
        }
        
    }
    //表头
#pragma mark -表头
    NSString * func = @"queryDispatchlist";
    func =  self.type == 0?@"queryDispatchlist":@"queryExConsignment";
    [HttpRequest requestPostWithParm:dic
                              method:func
                         startHander:^(NSProgress *downloadProgress) {
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _dicHeader =[NSDictionary dictionaryWithDictionary:array[0]];
                                     
                                     
                                     
                                     
                                     
                                     if (self.type == 1) { //出口
                                         if ([[_dicHeader allKeys] containsObject:@"isCredit"]) {
                                             NSInteger keyID =[_dicHeader [@"isCredit"] integerValue];
                                             if (keyID == 1) {
                                                 _array =@[@"客户",@"预计出货日",
                                                           @"出库金额",
                                                           @"信用证金额",@"结算条件",
                                                           @"部门",@"业务员",
                                                           @"制单人",@"实时应收账款",
                                                           @"预收金额",@"信用证编号",
                                                           @"开证日期",@"备注"];
                                             }
                                         } else {
                                             if (self.type == 0 && self.needShenpi == NO) {
                                                 _array = @[@"客户",@"预计出货日",
                                                            @"出库金额",@"结算条件",
                                                            @"部门",@"业务员",
                                                            @"制单人",@"信用额度",
                                                            @"备注"];
                                             }
                                         }
                                     }
                                     if ([[self getString:_dicHeader[@"cVerifier"]] length]) {
                                         NSMutableArray * array=[NSMutableArray arrayWithArray:_array];
                                         [array addObject:@"审核人"];
                                         _array =[ array copy];
                                     }
                                     [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
                                 }
                                 NSArray * list=result[@"listdata"];
                                 if (list) {
                                     _dicHeader =[NSDictionary dictionaryWithDictionary:list[0]];
                                     
                                     if (self.type == 1) { //出口
                                         if ([[_dicHeader allKeys] containsObject:@"isCredit"]) {
                                             NSInteger keyID =[_dicHeader [@"isCredit"] integerValue];
                                             if (keyID == 1) {
                                                 _array =@[@"客户",@"预计出货日",
                                                           @"出库金额",
                                                           @"信用证金额",@"结算条件",
                                                           @"部门",@"业务员",
                                                           @"制单人",@"实时应收账款",
                                                           @"预收金额",@"信用证编号",
                                                           @"开证日期",@"备注"];
                                             }
                                         }
                                     }
                                     if ([[self getString:_dicHeader[@"cVerifier"]] length]) {
                                         NSMutableArray * array=[NSMutableArray arrayWithArray:_array];
                                         [array addObject:@"审核人"];
                                         _array =[ array copy];
                                     }
                                     [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    //出货明细
#pragma mark -出货明细
    NSString * funDetail = @"queryDispatchlistdetail";
    if (self.type == 1) {
        funDetail = @"queryExConsignmentdetail";
    }
    [HttpRequest requestPostWithParm:dic
                              method:funDetail
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _chuhuoDetail =[NSArray arrayWithArray:array];
                                     [_tableView reloadData];
                                 }
                                 NSArray * list=result[@"listdata"];
                                 if (list) {
                                     _chuhuoDetail =[NSArray arrayWithArray:list];
                                     [_tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationTop];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    //发货单应收账龄表
#pragma mark -单应收账龄表
    NSString *funcArage = @"queryDispatchArAge";
    if (self.type == 1) {
        funcArage = @"queryExConsignmentArAge";
    }
    [HttpRequest requestPostWithParm:dic
                              method:funcArage
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _yingShouDic =[NSDictionary dictionaryWithDictionary:array[0]];
                                     [_tableView reloadData];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    //发货预收款
    NSString * funcPro = @"queryDispatchlistArPre";
    if (self.type == 1) {
        funcPro = @"queryExConsignmentArPre";
    }
#pragma mark -发货预收款
    [HttpRequest requestPostWithParm:dic
                              method:funcPro
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _yushouDic =[NSArray arrayWithArray:array];
                                     [_tableView reloadData];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    
}
-(void)notSave{
    [self showShenpi:@"2" title:@"驳回" Liyou:@"驳回"];
}
#pragma mark 弃审
- (void)notSave2{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"弃审", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary * dic=[NSMutableDictionary dictionary];
		if (self->_liyouField.text.length) {
			[dic setObject:self->_liyouField.text forKey:@"opinion"];
        }
		if (self.order_id.length) {
			[dic setObject:self.order_id forKey:@"Dlid"];
		}
        
		if ([self->_dicHeader[@"ccode"] length]) {
            
			[dic setValue:self->_dicHeader[@"ccode"] forKey:@"Dlcode"];//QC17060017
        }
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //弃审
        NSString * func = @"ExConsignment_abandon";
        if (self.type == 0) {
            func = @"Dispatchlist_abandon";
        }
        [HttpRequest requestPostWithParm:dic
                                  method:func
                             startHander:^(NSProgress *downloadProgress) {
                                 
                             } success:^(NSDictionary *result) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 
                                 NSLog(@"%@",result.description);
                                 if ([result[@"returnCode"] integerValue] == 10000) {
                                     //showFadeOutView(@"审核成功", NO, 2);
                                     
                                     
                                     [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                                         make.left.right.top.mas_equalTo(0);
                                         make.bottom.mas_equalTo(self.view.mas_bottom);
                                     }];
                                     [_footerView mas_updateConstraints:^(MASConstraintMaker *make) {
                                         make.left.bottom.right.mas_equalTo(0);
                                         make.height.mas_equalTo(0);
                                     }];
                                     NSString * message = @"";
                                     if ([result[@"returnCode"] integerValue] == 10000) {
                                         NSLog(@"%@",result.description);
                                         message = @"弃审成功";
                                         NSString * shenhe = result[@"Desc"];
                                         if (shenhe.length) {
                                             NSArray * array = [shenhe componentsSeparatedByString:@" "];
                                             message =[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(array[0],nil),array[1],NSLocalizedString(array[2],nil)];
                                         }
                                     } else {
                                         message = @"弃审失败，联系系统管理员";
                                     }
                                     [self showAlertMessage:message clickBlock:^{
                                         [self doSomeThingWhenShenpi];
                                         
                                         [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                     }];
                                 }else {
                                     [self showAlertMessage:@"弃审失败，联系系统管理员" clickBlock:^{
                                         [self doSomeThingWhenShenpi];
                                         
                                         [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                     }];
                                 }
                             } fialHander:^(NSString *error) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self showAlertMessage:@"网络延时" clickBlock:^{
                                     [self doSomeThingWhenShenpi];
                                     [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                 }];
                                 
                             } needPwd:YES];
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = NSLocalizedString(@"弃审", nil);
        _liyouField = textField;
        textField.returnKeyType = UIReturnKeyDone;
    }];
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];

}
-(void)showShenpi:(NSString *)statue title:(NSString *)title Liyou:(NSString*)liyou{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self pizhunString:_liyouField.text status:statue];
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = NSLocalizedString(liyou, nil);
        _liyouField = textField;
        textField.returnKeyType = UIReturnKeyDone;
    }];
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark 批准
-(void)save{
    [self showShenpi:@"1" title:@"批准" Liyou:@"批准"];
}
#pragma mark -UITabelView DataSource


-(void)pizhunString:(NSString *)str status:(NSString*)code{
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (_liyouField.text.length) {
        [dic setObject:_liyouField.text forKey:@"opinion"];
    }
	if (code.length) {
		[dic setObject:code forKey:@"ApproveState"];
	}
	if (self.order_id.length) {
		[dic setObject:self.order_id forKey:@"Dlid"];//self.id
	}
    if ([_dicHeader[@"ccode"] length]) {
        [dic setValue:_dicHeader[@"ccode"] forKey:@"Dlcode"];//_dicHeader[@"ccode"]
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString  * func = @"Dispatchlist_approve";//ExConsignment_approve
    if (self.type == 1) {
        func = @"ExConsignment_approve";
    }
    [HttpRequest requestPostWithParm:dic
                              method:func
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             
                             NSLog(@"%@",result.description);
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 //showFadeOutView(@"审核成功", NO, 2);
                                 
                                 
                                 [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                                     make.left.right.top.mas_equalTo(0);
                                     make.bottom.mas_equalTo(self.view.mas_bottom);
                                 }];
                                 [_footerView mas_updateConstraints:^(MASConstraintMaker *make) {
                                     make.left.bottom.right.mas_equalTo(0);
                                     make.height.mas_equalTo(0);
                                 }];
                                 NSString * message = @"";
                                 if ([result[@"returnCode"] integerValue] == 10000) {
                                     NSLog(@"%@",result.description);
                                      message = @"审核成功";
                                     NSString * shenhe = result[@"Desc"];
                                     if (shenhe.length) {
                                         NSArray * array = [shenhe componentsSeparatedByString:@" "];
                                         message =[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(array[0],nil),array[1],NSLocalizedString(array[2],nil)];
                                     }
                                     if ([code isEqualToString:@"2"]) {
                                         message = @"驳回成功";
                                     }
                                     
                                 } else {
                                     message = @"审核失败，联系系统管理员";
                                 }
                                 [self showAlertMessage:message clickBlock:^{
                                     [self doSomeThingWhenShenpi];
                                     
                                     [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                 }];
                             }else {
                                 [self showAlertMessage:@"审核失败，联系系统管理员" clickBlock:^{
                                     [self doSomeThingWhenShenpi];
                                     
                                     [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                 }];
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             [self showAlertMessage:@"网络延时" clickBlock:^{
                                 [self doSomeThingWhenShenpi];
                                 [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                             }];
                             
                         } needPwd:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:return  _array.count;break;
        case 1:return _chuhuoDetail.count + 1;break;
        case 2: return 1;break;
        case 3:return _yushouDic.count;break;
        default:
            break;
    }
    return 13;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
        {
            FYSaleHeaderView * view = [[FYSaleHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46) title:@"出货明细" ImageName:@"shippingDetail"];
            return view;
            
        }
            break;
        case 2:{
            FYSaleHeaderView * view = [[FYSaleHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46) title:@"应收账款账龄表" ImageName:@"receivables"];
            return view;
        }
            break;//AdvancePayments@2x
        case 3:{
            FYSaleHeaderView * view = [[FYSaleHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46) title:@"预收款" ImageName:@"AdvancePayments"];
            return view;
        }
            break;//AdvancePayments@2x
            
        default:
            break;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
            return 46;
            break;
        case 2:
            return 46;
            break;
        case 3:
            return 46;
            break;
            
            
            
        default:
            break;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:return  30;break;
        case 1: return indexPath.row == _chuhuoDetail.count ?  54 : 147;break;
        case 2: return 116;break;
        case 3:return 69;break;
        default:
            break;
    }
    return 13;}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // NSString * format= @"###,##0.00";
    switch (indexPath.section) {
        case 0:
        {
            FYSaleHeaderCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"saleheadercell" forIndexPath:indexPath];
            cell.titleLabel.text = NSLocalizedString(_array[indexPath.row],nil);
            NSString * content = @"";
            switch (indexPath.row) {
                    /*
                     信用证   正常的
                     0@"客户", @"客户"
                     1@"预计出货日", @"预计出货日"
                     2@"出库金额", @"出库金额"
                     3@"信用证金额", @"结算条件"
                     4@"结算条件", @"部门
                     5@"部门", @"业务员
                     6@"业务员", @"制单人
                     7@"制单人", @"信用额度"
                     8@"实时应收账款", ,@"实时应收账款
                     9@"预收金额",@"预收金额"
                     10@"信用证编号",@"信用余额
                     11@"开证日期",@"备注"
                     12@"备注"*/
                    /*
                     @[@"客户",@"预计出货日",
                     @"出库金额",@"结算条件",
                     @"部门",@"业务员",
                     @"制单人",@"信用额度",
                     @"备注"];
                     */
                case 0:content = _dicHeader[@"ccusname"]; break;//客户
                case 1:content = _dicHeader[@"dPreDate"]; break;//出库日
                    // case 2:content = [self getString: _dicHeader[@"cexch_name"]]; break;//币种
                case 2:content = [self getString:_dicHeader[@"outstockmny"]]; break;//出库金额
                case 3:{
                    if ([self needShowListTable]) {
                        content =[self monery: [self getString:_dicHeader[@"cCreditAmount"]]]; break;//信用证金额
                    } else {
                        content = [self getString:_dicHeader[@"accway"]]; break;//结算条见
                    }
                }
                case 4:{if ([self needShowListTable]) {
                    content = [self getString:_dicHeader[@"accway"]]; break;//结算条件
                }else {
                    content = [self getString:_dicHeader[@"cdepname"]]; break;//部门
                }
                    
                }
                case 5:{if ([self needShowListTable]){
                    content = [self getString:_dicHeader[@"cdepname"]]; break;//部门
                }else{
                    content = [self getString:_dicHeader[@"cpersonname"]]; break;//业务员
                }
                    
                }
                case 6:{
                    if ([self needShowListTable]){
                        content = [self getString:_dicHeader[@"cpersonname"]]; break;//业务员
                    }else {
                        content = [self getString:_dicHeader[@"cmaker"]]; break;//制单员
                    }
                }
                case 7:{
                    if ([self needShowListTable]) {
                        content = [self getString:_dicHeader[@"cmaker"]]; break;//制单员
                    } else {
                        content = [self monery:[NSString stringWithFormat:@"%ld", [_dicHeader[@"iCusCreLine"] integerValue]]];
                    }
                } break;//信用额度
                case 8:{
                    if ( self.needShenpi == NO) {
                        content = [self getString:_dicHeader[@"memo"]]; break;//备注
                    } else{
                       // content =[self monery: [self getString:_dicHeader[@"rtCAmount"]]]; break;//实时应收账款
                        //实时应收账款
                         content =[self monery: [self getString:_dicHeader[@"rtCAmount"]]]; break;
                    }
                }
                case 9:{
                    content =[self monery: [self getString:_dicHeader[@"preCAmount"]]];//实时应收账款
                    //                    if ([self needShowListTable]) {
                    //                        content = [self getString:_dicHeader[@"cCreditNo"]];//信用编号
                    //                    }else {
                    //                        content =[self monery:[self getString:_dicHeader[@"creditbalance"]]];
                    //                    }
                } break;//信用余额
                case 10:{
                    if ([self needShowListTable]) {
                        content = [self getString:_dicHeader[@"cCreditNo"]]; break;//开证日期
                    }else {
                        content =[self monery: [self getString:_dicHeader[@"creditbalance"]]]; break;//信用余额
                    }
                }
                case 11:{if ([self needShowListTable]){
                    content = [self getString:_dicHeader[@"cCreditDate"]]; break;//日期
                }else{
                    content = [self getString:_dicHeader[@"memo"]]; break;//备注
                }
                    
                }
                case 12:if ([self needShowListTable]){
                    content = [self getString:_dicHeader[@"memo"]]; break;//备注
                }else{
                    content = [self getString:_dicHeader[@"cVerifier"]]; break;//审核人
                }
                default:content = [self getString:_dicHeader[@"cVerifier"]]; break;//审核人
                    break;
            }
            cell.contentLabel.text = content;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        case 1:{
            if (indexPath.row != _chuhuoDetail.count) {
                FYSaleDetailSecondCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYSaleDetailSecondCell" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                NSDictionary * info = _chuhuoDetail[indexPath.row];
                
                [cell configDic:info];
				if (_type == 1) {
					cell.danjiaLabel.text = [self strWithWaibi:info[@"ftaxprice"] category:info[@"cexch_name"]];
				}else if(_type == 0){
					cell.danjiaLabel.text = [self strWithWaibi:info[@"itaxunitprice"] category:info[@"cexch_name"]];
				}
                cell.yuanbiMoneryLabel.text = [self strWithRMB:info[@"iNatSum"]
                                                         waibi:info[@"iSum"]
													  category:info[@"cexch_name"]];
                if (self.type == 1) {
                    cell.yuanbiMoneryLabel.text = [self strWithRMB:info[@"fnattaxmoney"]
                                                             waibi:info[@"ftaxmoney"] category:info[@"cexch_name"]];
                    cell.shuliangLabel.text = info[@"fquantity"] ;
                }
                return cell;
            } else {
				//合计金额展示
                FYSaleHejiCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYSaleHejiCell" forIndexPath:indexPath];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.chuhuojineLabel.text = [self getSumMonery];
                return cell;
            }
        }break;
        case 2:{
            FYShouldSaveTableViewCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYShouldSaveTableViewCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell configDic:_yingShouDic];
            return cell;
        }break;
        case 3:{
            FYReceivedTableViewCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYReceivedTableViewCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell configDIc:_yushouDic[indexPath.row]];
            return cell;
        }break;
            
        default:
            break;
    }
    FYSaleHeaderCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"saleheadercell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleLabel.text =NSLocalizedString(_array[indexPath.row], nil) ;
    cell.contentLabel.text = @"XXXXXXXXXXXXXXXXXXXX";
    return cell;
}
-(NSString *)getString:(NSString *)str{
    if ([str isKindOfClass:[NSNull class]]) {
        return @"";
    }
    if ([str length]) {
        return str;
    }
    return str;
}
#pragma mark -需要信用编号
-(BOOL)needShowListTable{
    if (self.type == 1) {
        NSInteger keyID =[_dicHeader [@"isCredit"] integerValue];
        if (keyID == 1){
            return YES;
        }
    }
    return NO;
    
}
#pragma mark -UITabelView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
-(NSString *)strWithRMB:(NSString *)str waibi:(NSString *)waibi category:(NSString *)category{
    NSString * format= @"###,##0.00";
    if ([category isEqualToString:@"JPY"]) {
        format = @"###,##0";
    }
    NSString * waibiStr =[self monery:waibi WithForamt:format];
    NSString * benbi =[self monery:str WithForamt:@"###,##0.00"];
    NSString * last=[NSString stringWithFormat:@"%@ %@/%@ CNY",waibiStr,category,benbi];
    return last;
}
-(NSString *)strWithWaibi:(NSString *)waibi category:(NSString *)category{
    NSString * format= @"###,##0.00";
    if ([category isEqualToString:@"JPY"]) {
        format = @"###,##0";
    }
    NSString * waibiStr =[self monery:waibi WithForamt:format];
    NSString * last=[NSString stringWithFormat:@"%@ %@",waibiStr,category];
    return last;
}
-(NSString * )monery:(NSString *)monery WithForamt:(NSString *)format{
    CGFloat Monery= [monery floatValue];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:format];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:Monery]];
    return formattedNumberString;
}
-(NSString *)getSumMonery{
    CGFloat rmb = 0;
    CGFloat us = 0;
    for (NSInteger i = 0; i < _chuhuoDetail.count; i ++) {
        NSDictionary * info = _chuhuoDetail[i];
        CGFloat rmbIn=[info[@"iNatSum"]  floatValue];
        CGFloat usIn=[info[@"iSum"]  floatValue];
        if (self.type == 1) {
            rmbIn=[info[@"fnattaxmoney"]  floatValue];
            usIn=[info[@"ftaxmoney"]  floatValue];
        }
        rmb += rmbIn;
        us += usIn;
    }
    if (_chuhuoDetail.count) {
        NSString * last = [self strWithRMB:[NSString stringWithFormat:@"%f",rmb] waibi:[NSString stringWithFormat:@"%f",us] category:_chuhuoDetail[0][@"cexch_name"]];
        return last;
    } else {
        return @"0";
    }
    
}
@end
