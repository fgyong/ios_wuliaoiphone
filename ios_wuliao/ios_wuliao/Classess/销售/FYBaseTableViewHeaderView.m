//
//  FYBaseTableViewHeaderView.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseTableViewHeaderView.h"
@interface FYBaseTableViewHeaderView ()


@property (nonatomic,strong) UIImageView *arrow;

@property (nonatomic,copy) clickBlock block;
@property (nonatomic,strong) FYLable * line;

@end
@implementation FYBaseTableViewHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.arrow];
        self.arrow.frame = CGRectMake(13, 12, 9, 16);
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.NumberLabel];
        
        
        
        UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)];
        [self.contentView addGestureRecognizer:tap];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.line];
        self.frame = CGRectMake(0, 0, kScreenWidth, 40);
    }
    return self ;
}
-(void)title:(NSString *)title
        open:(BOOL)isOpen{
    self.titleLabel.text = title;
    [self setOpenStatus:isOpen];
}
-(UIImageView *)arrow{
    if (_arrow == nil) {
        _arrow =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"triangleRight"]];
        [self.contentView addSubview:_arrow];
        _arrow.frame = CGRectMake(13, 12, 9, 16);
    }
    return _arrow;
}
-(FYLable *)line{
    if (_line == nil) {
        _line=[FYLable lineWithFrame:CGRectMake(0, 39, kScreenWidth, 0.5)];
    }
    return _line;
}
-(FYLable *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel=[[FYLable alloc]initWithFrame:CGRectMake(34, 11.5, 150, 17)];
       
        _titleLabel.font = Font(15);
        _titleLabel.textColor = COLOR_Subject_Color;
       
    }
    return _titleLabel;
}
-(FYLable *)NumberLabel{
    if (_NumberLabel == nil) {
        _NumberLabel=[[FYLable alloc]initWithFrame:CGRectMake(kScreenWidth - 15 - 25, 12.5, 25, 15)];
        _NumberLabel.clipsToBounds = YES;
        _NumberLabel.layer.cornerRadius = 8;
        _NumberLabel.textAlignment = NSTextAlignmentCenter;
        _NumberLabel.font = Font(13);
        _NumberLabel.textColor =[UIColor whiteColor];
        _NumberLabel.backgroundColor = COLOR_Subject_Color;
        _NumberLabel.adjustsFontSizeToFitWidth = YES;
    }
    return _NumberLabel;
}
-(void)setOpenStatus:(BOOL)isOpen{
    self.open = isOpen;
    [UIView animateWithDuration:0.25 animations:^{
        if (isOpen == NO) {
            self.arrow.image =[ UIImage imageNamed:@"triangleDown"];
            _arrow.frame = CGRectMake(9.5, 15.5, 16, 9);
        } else {
            self.arrow.image =[ UIImage imageNamed:@"triangleRight"];
            _arrow.frame = CGRectMake(13, 12, 9, 16);
        }
    }];

}
-(void)setClickBlock:(clickBlock)block{
    if (block) {
        self.block= block;
    }
}
-(void)click:(UITapGestureRecognizer *)tap{
    
    
    if (self.block) {
        self.block(self.tag);
        
    }
}
@end
