//
//  FYBaseCellTableViewCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseCellTableViewCell.h"

@implementation FYBaseCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configDIc:(NSDictionary *)info{
    self.titleLabel.text = [self getString: info[@"ccusname"]];
    self.bumenLabel.text =  [self getString:info[@"cdepname"]];
    self.yewuYuanLabel.text=  [self getString:info[@"cpersonname"]];
   
    self.chukuriLabel.text=  [[self getString:info[@"dPreDate"]] componentsSeparatedByString:@" "][0];
    self.cangku.text =   [self getString:info[@"cwhname"]];
    self.jineLabel.text =   [self getString: info[@"outstockmny"]];//出库金额
    self.tiaojianLabel.text =  [self getString: info[@"accway"]];
    self.danHao.text =  [self getString: info[@"ccode"]];

}
@end
