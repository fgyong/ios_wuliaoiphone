//
//  FYSelectViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/30.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYSelectCell.h"
#import "FYSelectSecondVC.h"


@interface FYSelectViewController : FYBaseViewController

@property (nonatomic,strong) NetSuccessHander success;
-(void)successBlock:(NetSuccessHander)block;

@end
