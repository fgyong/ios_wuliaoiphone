//
//  FYHeaderView.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/28.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^clickBlock)(NSInteger number);

@interface FYHeaderView : UITableViewHeaderFooterView

@property (assign) BOOL open;

-(void)title:(NSString *)title
   imageName:(NSString *)name
      number:(NSString *)number
  openStatus:(BOOL)isOpen;

-(void)title:(NSString *)title
        open:(BOOL)isOpen;
-(void)setClickBlock:(clickBlock)block;

-(void)setOpenStatus:(BOOL)isOpen;
@end
