//
//  FYLoginViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/28.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYLoginViewController.h"
#import "FYPickerView.h"

@interface FYLoginViewController ()
{
    FYDatePickerView * _picker;
    
    UITextField *_oldTextfield;
    UITextField *_newTextfield;
    UITextField *_newTextfield1;
}
@end

@implementation FYLoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
    NSString * name = [[NSUserDefaults standardUserDefaults] objectForKey:@"cUserCode"];
    if ([name length]) {
        self.caozuoyuanTextField.text = name;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _picker =[[FYDatePickerView alloc]init];
    _picker.pickerView.datePickerMode = UIDatePickerModeDate;
    [_picker selectSuccess:^(NSString *dateString) {
        self.riqiField.text = dateString;
    }];
    NSDateFormatter * format=[NSDateFormatter new];
    [format setDateFormat:@"yyyy-MM-dd"];
    self.riqiField.text =[format stringFromDate:[NSDate date]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardFrameChange:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}
-(void)keyBoardFrameChange:(NSNotification *)obj{
    NSString * name = [obj name];
  
    if ([name isEqualToString:UIKeyboardWillShowNotification]) {
        [UIView animateWithDuration:0.25 animations:^{
            self.iconTopLayout.constant = -100;
            [self.view layoutIfNeeded];
            [self.view setNeedsDisplay];
        }];

    }else if([name isEqualToString:UIKeyboardWillHideNotification]){
        [UIView animateWithDuration:0.25 animations:^{
            self.iconTopLayout.constant = 69;
            [self.view layoutIfNeeded];
            [self.view setNeedsDisplay];
        }];
    }
}

- (IBAction)changeAccountType:(id)sender {
    NSLog(@"日期");
    [_picker show];
}
- (IBAction)login:(id)sender {
//    if (DEBUG) {
//        AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
//        [appdelegate allocVC];
//        return;
//    }
    NSLog(@"登陆");
    if (self.caozuoyuanTextField.text.length == 0) {
        showFadeOutView(@"账号不能为空", NO, 2);
        return;
    }
    if (self.pwdTextField.text.length == 0) {
        showFadeOutView(@"密码不能为空", NO, 2);
        return;
    }
    if (self.zhangtaoTextField.text.length == 0) {
        showFadeOutView(@"日期不能为空", NO, 2);
        return;
    }
    [[ UserInfoCache share] clearCache];
    NSDictionary * dic = @{@"cUserCode":self.caozuoyuanTextField.text,
                           @"cPassword":self.pwdTextField.text,
                           @"cAccid":@"008",
                           @"dOpDate":self.riqiField.text};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];//LoginAuthentication queryUserName
    [HttpRequest requestPostWithParm:dic
                              method:@"queryUserName"
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([result[@"returnCode"] integerValue] == 10000) {

            [JPUSHService setTags:[[NSSet alloc] initWithObjects:self.caozuoyuanTextField.text, nil]
                            alias:nil
                 callbackSelector:@selector(tagsAliasCallback:tags:alias:)
                           object:self];
            
            [[UserInfoCache share]setLocalNamePwd:dic];
            [[UserInfoCache share] setUserInfo:result forKey:@"userName"];
            
            [[NSUserDefaults standardUserDefaults] setObject:self.caozuoyuanTextField.text forKey:@"cUserCode"];
            AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
            [appdelegate allocVC];
        } else {
            showFadeOutView(@"登录用户名／密码错误", NO, 2);
        }
    } fialHander:^(NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needPwd:NO];
}
- (void)networkDidSetup:(NSNotification *)notification {
    NSLog(@"已连接");
}

- (void)networkDidClose:(NSNotification *)notification {
    NSLog(@"未连接");
}

- (void)networkDidRegister:(NSNotification *)notification {
    NSLog(@"已注册");
}

- (void)networkDidLogin:(NSNotification *)notification {
    NSLog(@"已登录");
    
    if ([JPUSHService registrationID]) {
        NSLog(@"get RegistrationID");
    }
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *content = [userInfo valueForKey:@"content"];
    NSLog(@"%@", content);
}

- (void)serviceError:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *error = [userInfo valueForKey:@"error"];
    NSLog(@"%@", error);
}

- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    NSString *callbackString =
    [NSString stringWithFormat:@"%d, \ntags: %@, \nalias: %@\n", iResCode,
     [self logSet:tags], alias];
    NSLog(@"TagsAlias回调:%@", callbackString);
}


- (NSString *)logSet:(NSSet *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (IBAction)changePwd:(id)sender {
    NSLog(@"改变密码");
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"修改密码", nil)
                                                                   message:nil
                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认修改", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (_caozuoyuanTextField.text.length == 0) {
            showFadeOutView( @"账号不能为空",NO, kDelayTime);
            return ;
        }
        
        if ([_newTextfield.text isEqualToString:_newTextfield1.text] == NO) {
            showFadeOutView( @"新密码两次输入不一致",NO, kDelayTime);
            return;
        }
        
        if ([_newTextfield.text length] == 0) {
            showFadeOutView( @"新密码不能为空",NO, kDelayTime);
            return;
        }
 
        NSString * newpwd = _newTextfield.text;
        NSString  *old = _oldTextfield.text;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [HttpRequest requestPostWithParm:@{@"newpwd":newpwd,
                                           @"cPassword":old,
                                           @"cUserCode":self.caozuoyuanTextField.text,
                                           @"cAccid":@"008",
                                           @"dOpDate":self.riqiField.text}
                                  method:@"LoginAndChangePwd"
                             startHander:^(NSProgress *downloadProgress) {
                                 
                             } success:^(NSDictionary *result) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 
                                 if ([result[@"returnCode"] integerValue] == 10000) {
                                     NSLog(@"%@",result.description);
                                     [self loginWithDic:@{@"cPassword":newpwd,
                                                          @"cUserCode":self.caozuoyuanTextField.text,
                                                          @"cAccid":@"008",
                                                          @"dOpDate":self.riqiField.text}];
//                                     showFadeOutView(@"操作成功", NO, kDelayTime);
                                 } else if([result[@"returnCode"] integerValue] == 10008) {
                                     if ([NSObject isJapanceLangue]== NO ) {
                                         showFadeOutView(result[@"errDesc"], NO, 2);
                                     } else {
                                         showFadeOutView(@"新しいパスワードの長さ、複雑さ、または履歴に関するERPセキュリティの要件を満たしていません", NO, 2);
                                     }
                                 } else if([result[@"returnCode"] integerValue] == 10007) {
                                         showFadeOutView(@"用户名或密码错误", NO, 2);
                                 }else {
                                     showFadeOutView(result[@"errDesc"], NO, 2);
                                 }
                             } fialHander:^(NSString *error) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                             } needPwd:YES];


    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"旧密码", nil);
        _oldTextfield = textField;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"新密码", nil);
//        textField.secureTextEntry = YES;
        _newTextfield = textField;
    }];
    [alert addAction:alerttion];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"新密码", nil);
        
        _newTextfield1 = textField;
    }];
    
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)loginWithDic:(NSDictionary *)info{
    [HttpRequest requestPostWithParm:info
                              method:@"queryUserName"
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([result[@"returnCode"] integerValue] == 10000) {
            
            [JPUSHService setTags:[[NSSet alloc] initWithObjects:self.caozuoyuanTextField.text, nil]
                            alias:nil
                 callbackSelector:@selector(tagsAliasCallback:tags:alias:)
                           object:self];
            [[UserInfoCache share] clearCache];//清除之前的缓存
            [[UserInfoCache share] setLocalNamePwd:info];
            [[UserInfoCache share] setUserInfo:result forKey:@"userName"];
            AppDelegate * appdelegate =(AppDelegate *) [[UIApplication sharedApplication] delegate];
            [appdelegate allocVC];
        } else {
            showFadeOutView(@"用户名或密码错误", NO, 2);
        }
    } fialHander:^(NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needPwd:NO];

}
@end
