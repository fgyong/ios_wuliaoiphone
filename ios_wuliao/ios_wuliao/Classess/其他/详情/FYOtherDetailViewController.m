//
//  FYOtherDetailViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYOtherDetailViewController.h"

@interface FYOtherDetailViewController ()
<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSArray *_array;
    NSDictionary * _dicHeader;
    NSArray *_chuhuoDetail;
    UITextField * _liyouField;
    
    UIView *_footerView;
    
    BOOL _isNeedShowYuJiJinE;//是否需要预计金额表头和标体
    NSArray *_cellKeys;//cell取值的key
}
@end

@implementation FYOtherDetailViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[FYKeyBoardManger shareManger] setValible:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[FYKeyBoardManger shareManger] setValible:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setMMDBackItem:NO];
//    if (self.titleStr) {
//        [self setMMDTitle:self.titleStr];
//    } else {
//        [self setMMDTitle:@"其他审核"];
//    }
    if (self.needShenpi == NO) {
        [self setMMDTitle:@"已审核"];
    } else {
        [self setMMDTitle:@"未审核"];
    }
//    _isNeedShowYuJiJinE = YES;//默认显示预计出库金额
    [self getDataFromServer];
}
-(void)DoSomeThingWhenSHenpi{
    if ([self.delegate respondsToSelector:@selector(shenpiStatusComplate)]) {
        [self.delegate performSelector:@selector(shenpiStatusComplate)];
    }
}
-(void)configUI{
    _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableHeaderView = nil;
    _tableView.backgroundColor = Color_DefaultBackground;
    [self.view addSubview:_tableView];
    if (_type == 2) {//2是调拨详情
        _array = @[@"转出仓库",@"转入仓库",@"转出部门",@"转入部门",@"调拨日期",@"业务员",@"制单人",@"备注"];
    } else {//1是其他详情
        _array = @[@"客户",@"出货日",@"出货仓库",@"部门",@"业务员",@"制单人",@"收货地址",@"预估出库金额",@"备注"];
    }
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHeaderCell" bundle:nil] forCellReuseIdentifier:@"saleheadercell"];
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleDetailSecondCell" bundle:nil] forCellReuseIdentifier:@"FYSaleDetailSecondCell"];
    //FYSaleHejiCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYSaleHejiCell" bundle:nil] forCellReuseIdentifier:@"FYSaleHejiCell"];
    //FYShouldSaveTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYShouldSaveTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYShouldSaveTableViewCell"];
    //FYReceivedTableViewCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYReceivedTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYReceivedTableViewCell"];
    
    //FYAllocDetailCell
    [_tableView registerNib:[UINib nibWithNibName:@"FYAllocDetailCell" bundle:nil] forCellReuseIdentifier:@"FYAllocDetailCell"];
    
    _tableView.estimatedRowHeight = 21;
    CGFloat bottom = - 50;
    if (self.needShenpi == NO) {
        bottom = -50;
    }
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(6);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(bottom);
    }];
    UIView * footerView=[UIView new];
    footerView.backgroundColor =[UIColor whiteColor];
    
    UIView * lineTop=[[UIView alloc]initWithFrame:CGRectZero];
    lineTop.backgroundColor = Color_DefaultBackground;
    [footerView addSubview:lineTop];
    [lineTop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(footerView);
        make.right.mas_equalTo(footerView);
        make.top.mas_equalTo(footerView);
        make.height.mas_equalTo(6);
    }];
    CGFloat bottomfooter =  50;
    [self.view addSubview:footerView];
    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(bottomfooter);
    }];
    if (self.needShenpi == NO) {//历史记录不用审批
        [_tableView addSubview:[self checkImageViewStatus:1]];
        UIButton * btn=[UIButton new];
        [btn setTitle:NSLocalizedString(@"弃审", nil) forState:UIControlStateNormal];
        
        btn.clipsToBounds = YES;
        [footerView addSubview:btn];
        btn.backgroundColor = COLORRGB(243, 101, 23);
        [btn addTarget:self action:@selector(notSave2) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = Font(16);
        
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(120, 28));
            make.top.mas_equalTo(lineTop.mas_bottom).offset(8);
            make.centerX.mas_equalTo(footerView.mas_centerX);
        }];
        return;
    }
    
    UIButton * btn=[UIButton new];
    [btn setTitle:NSLocalizedString(@"驳回", nil) forState:UIControlStateNormal];
    
    btn.clipsToBounds = YES;
    btn.backgroundColor = COLORRGB(243, 101, 23);
    [footerView addSubview:btn];
    [btn addTarget:self action:@selector(notSave) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = Font(16);
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 28));
        make.top.mas_equalTo(lineTop.mas_bottom).offset(8);
        make.right.mas_equalTo(footerView.mas_centerX).offset(-20);
    }];
    
    UIButton * btn2=[UIButton new];
    [btn2 setTitle:NSLocalizedString(@"批准", nil) forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor whiteColor] forState:0];
    btn2.clipsToBounds = YES;
    btn2.backgroundColor = COLOR_Subject_Color;
    btn2.titleLabel.font = Font(16);
    [footerView addSubview:btn2];
    [btn2 addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 28));
        make.top.mas_equalTo(btn);
        make.left.mas_equalTo(self.view.center.x + 20);
    }];
    
    _footerView = footerView;
  
    //[self.view bringSubviewToFront:_tableView];
//    [footerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.view);
//        make.right.mas_equalTo(self.view);
//        make.bottom.mas_equalTo(self.view);
//        make.height.mas_equalTo(50);
//    }];
}
- (void)notSave2{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"弃审", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary * dic=[NSMutableDictionary dictionary];
        if (_liyouField.text.length) {
            [dic setObject:_liyouField.text forKey:@"opinion"];
        }
        
        [dic setObject:self.order_id forKey:@"Dlid"];
        
        if ([_dicHeader[@"ccode"] length]) {
            
            [dic setValue:_dicHeader[@"ccode"] forKey:@"Dlcode"];//QC17060017
        }
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //弃审
        NSString * func = @"RdRecord09_abandon";
        if (self.type == 2) {
            func  = @"TrunsVouch_abandon";
        }
        [HttpRequest requestPostWithParm:dic
                                  method:func
                             startHander:^(NSProgress *downloadProgress) {
                                 
                             } success:^(NSDictionary *result) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 
                                 // [[NSNotificationCenter defaultCenter] postNotificationName:@"postName" object:nil];
                                 
                                 if ([result[@"returnCode"] integerValue] == 10000) {
                                     NSLog(@"%@",result.description);
                                     [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                                         make.left.right.top.mas_equalTo(0);
                                         make.bottom.mas_equalTo(self.view.mas_bottom);
                                     }];
                                     [self.view bringSubviewToFront:_tableView];
                                 }
                                 NSString * message = @"";
                                 if ([result[@"returnCode"] integerValue] == 10000) {
                                     NSLog(@"%@",result.description);
                                     message = @"弃审成功";
                                 } else {
                                     message = @"弃审失败，联系系统管理员";
                                 }
                                 [self showAlertMessage:message clickBlock:^{
                                     [self DoSomeThingWhenSHenpi];
                                     
                                     [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                 }];
                             } fialHander:^(NSString *error) {
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self showAlertMessage:@"网络延时" clickBlock:^{
                                     [self DoSomeThingWhenSHenpi];
                                     
                                     [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                                 }];
                             } needPwd:YES];
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = NSLocalizedString(@"弃审", nil);
        _liyouField = textField;
        textField.returnKeyType = UIReturnKeyDone;
    }];
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)getDataFromServer{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //表头
    NSDictionary * dic=[NSMutableDictionary dictionary];
    if (self.order_id) {
        [dic setValue:self.order_id forKey:@"id"];
    }
#pragma mark -表头
    NSString * func = @"queryRdRecord09";
    func =  self.type == 2?@"queryTrunsVouch":@"queryRdRecord09";
    [HttpRequest requestPostWithParm:dic
                              method:func
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _dicHeader =[NSDictionary dictionaryWithDictionary:array[0]];
                                     if ([[self getString:_dicHeader[@"cVerifier"]] length]) {
                                         NSMutableArray * array=[NSMutableArray arrayWithArray:_array];
                                         [array addObject:@"审核人"];
                                         _array =[ array copy];
                                     }
//                                     是否显示   预计出库金额
                                     if ([[self getString: _dicHeader[@"crdcode"]] isEqualToString:@"1201"])
                                     {
                                         _isNeedShowYuJiJinE = YES;// 1201  显示
                                     }else {
                                         _isNeedShowYuJiJinE = NO;
                                     }
                                     [self configCellKeys];
                                     [_tableView reloadData];
                                 }
                                 NSArray * list=result[@"listdata"];
                                 if (list) {
                                     _dicHeader =[NSDictionary dictionaryWithDictionary:list[0]];
                                     if ([[self getString:_dicHeader[@"cVerifier"]] length]) {
                                         NSMutableArray * array=[NSMutableArray arrayWithArray:_array];
                                         [array addObject:@"审核人"];
                                         _array =[ array copy];
                                     }
                                     [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationTop];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    //出货明细
#pragma mark -出货明细
    NSString * funDetail = @"queryRdRecord09detail";
    if (self.type == 2) {
        funDetail = @"queryTrunsVouchdetail";
    }
    [HttpRequest requestPostWithParm:dic
                              method:funDetail
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 if (array.count) {
                                     _chuhuoDetail =[NSArray arrayWithArray:array];
                                     [_tableView reloadData];
                                 }
                                 NSArray * list=result[@"listdata"];
                                 if (list) {
                                     _chuhuoDetail =[NSArray arrayWithArray:list];
                                     [_tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationTop];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         } needPwd:YES];
    //发货单应收账龄表
}
-(void)notSave{
    [self showShenpi:@"2" title:@"驳回" Liyou:@"驳回"];
}
-(void)showShenpi:(NSString *)statue title:(NSString *)title Liyou:(NSString*)liyou{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(title, nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"取消", nil) style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                      }];
    UIAlertAction* okalerttion = [UIAlertAction actionWithTitle:NSLocalizedString(@"确认", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        dispatch_async(dispatch_get_main_queue(), ^{
             [self pizhunString:_liyouField.text status:statue];
        });
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = NSLocalizedString(liyou, nil);
        _liyouField = textField;
        textField.returnKeyType = UIReturnKeyDone;
    }];
    [alert addAction:alerttion];
    [alert addAction:okalerttion];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)save{
    [self showShenpi:@"1" title:@"批准" Liyou:@"批准"];
}
-(void)pizhunString:(NSString *)str status:(NSString*)code{
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (_liyouField.text.length) {
        [dic setObject:_liyouField.text forKey:@"opinion"];
    }
    [dic setObject:code forKey:@"ApproveState"];
    [dic setObject:self.order_id forKey:@"Dlid"];
 
    if ([_dicHeader[@"ccode"] length]) {
 
         [dic setValue:_dicHeader[@"ccode"] forKey:@"Dlcode"];//QC17060017
        //_dicHeader[@"ccode"]
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString * func = @"RdRecord09_approve";
    if (self.type == 2) {
        func = @"TrunsVouch_approve";
    }
    [HttpRequest requestPostWithParm:dic
                              method:func
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                            
                            // [[NSNotificationCenter defaultCenter] postNotificationName:@"postName" object:nil];
                             
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 [_tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                                     make.left.right.top.mas_equalTo(0);
                                     make.bottom.mas_equalTo(self.view.mas_bottom);
                                 }];
                                 [self.view bringSubviewToFront:_tableView];
                             }
                         
                             NSString * message = @"";
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 message = @"审核成功";
                                 if ([code isEqualToString:@"2"]) {
                                     message = @"驳回成功";
                                 }
                             } else {
                                 message = @"审核失败，联系系统管理员";
                             }
                             [self showAlertMessage:message clickBlock:^{
                                 [self DoSomeThingWhenSHenpi];

                                 [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                             }];
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             [self showAlertMessage:@"网络延时" clickBlock:^{
                                 [self DoSomeThingWhenSHenpi];
                                 
                                 [self performSelector:@selector(__popViewControllerAnimated) withObject:nil afterDelay:0.5];
                             }];
                         } needPwd:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:return  _array.count;break;
        case 1:return _chuhuoDetail.count;break;
            
        default:
            break;
    }
    return 13;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:return 30;break;

        case 1:{
            if (self.type == 2 || _isNeedShowYuJiJinE == NO) {
                return 75;//调拨
            }else {
                return 100;//其他
            }};break;
            
        default:
            break;
    }
    return 32;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
            return 46;
            break;
            
        default:
            break;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
        {
            FYSaleHeaderView * view = [[FYSaleHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 46) title:@"出货明细" ImageName:@"shippingDetail"];
            view.backgroundColor = [UIColor whiteColor];
            return view;
            
        }
        default:
            break;
    }
    return nil;
}
-(void)configCellKeys{
        _cellKeys = @[@"ccusname",
                           @"ddate",
                           @"cwh_name",/*出货仓库*/
                           //@"cDeliverUnit",/*出货地址*/
                           @"cdepname",/*部门cDeliverUnit*/
                           @"cpersonname",
                           @"cmaker",
                           @"cdefine11",//收货地址
                           @"cdefine16",//预估出库金额
                           @"cMemo",
                           @"cVerifier"];
        if (_isNeedShowYuJiJinE == NO) {//不需要出库金额的
            _cellKeys = @[@"ccusname",
                          @"ddate",
                          @"cwh_name",/*出货仓库*/
                          //@"cDeliverUnit",/*出货地址*/
                          @"cdepname",/*部门cDeliverUnit*/
                          @"cpersonname",
                          @"cmaker",
                          @"cdefine11",//收货地址
//                          @"cdefine16",//预估出库金额
                          @"cMemo",
                          @"cVerifier"];
            if (self.type != 2) {
                 _array = @[@"客户",@"出货日",@"出货仓库",@"部门",@"业务员",@"制单人",@"收货地址",@"备注"];
            }
        }
        if (self.type == 2) { // @"转出仓库",@"转入仓库",@"转出部门",@"转入部门",@"调拨日期",@"业务员",@"制单人",@"备注"
            _cellKeys = @[@"cOutwhname",
                     @"cInwhname",
                     @"cOutdepname",/*出货仓库*/
                     @"cIndepname",/*部门*/
                     @"ddate",
                     @"cpersonname",
                     @"cmaker",
                     @"cmemos",
                     @"cVerifier"];
        }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        FYSaleHeaderCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"saleheadercell" forIndexPath:indexPath];
        cell.titleLabel.text = NSLocalizedString(_array[indexPath.row],nil);
        NSString * content = @"";
//        NSArray * keys = @[@"ccusname",
//                           @"ddate",
//                           @"cwh_name",/*出货仓库*/
//                           //@"cDeliverUnit",/*出货地址*/
//                           @"cdepname",/*部门cDeliverUnit*/
//                           @"cpersonname",
//                           @"cmaker",
//                           @"cdefine11",//收货地址
//                           @"cdefine16",//预估出库金额
//                            @"cMemo",
//                           @"cVerifier"];
//        if (self.type == 2) { // @"转出仓库",@"转入仓库",@"转出部门",@"转入部门",@"调拨日期",@"业务员",@"制单人",@"备注"
//            keys = @[@"cOutwhname",
//                     @"cInwhname",
//                     @"cOutdepname",/*出货仓库*/
//                     @"cIndepname",/*部门*/
//                     @"ddate",
//                     @"cpersonname",
//                      @"cmaker",
//                     @"cmemos",
//                     @"cVerifier"];
//        }
        content = [self getString:_dicHeader[_cellKeys[indexPath.row]]];
        if (indexPath.row == 7 && _isNeedShowYuJiJinE && self.type != 2) {//第七行 是出库金额
            NSString * format= @"###,##0.00";
            NSString * allMoneryString=[NSString stringWithFormat:@"%@ CNY",[self monery:content WithForamt:format]];
            cell.contentLabel.text = allMoneryString;
        }else {
            cell.contentLabel.text = content;
        }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
        } else {
            FYAllocDetailCell  * cell =[tableView dequeueReusableCellWithIdentifier:@"FYAllocDetailCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setDic:_chuhuoDetail[indexPath.row]];
            if (self.type == 2 || _isNeedShowYuJiJinE == NO) {
                //其他的类型不隐藏 调拨单详情隐藏 其他类型的隐藏出库金额的
                cell.bottomLine.hidden = YES;
                cell.lastChuKuJineLabel.hidden = YES;
                cell.lastChuKuJinTextLabel.hidden = YES;
                cell.LayoutDistanceToBottomLine.constant = -22;
            }else {
                //其他的类型不隐藏 调拨单详情隐藏 其他类型的隐藏出库金额的
                cell.bottomLine.hidden = NO;
                cell.lastChuKuJineLabel.hidden = NO;
                cell.lastChuKuJinTextLabel.hidden = NO;
                cell.LayoutDistanceToBottomLine.constant = 5;
            }
            return cell;
        }
    return [UITableViewCell new];
}
@end
