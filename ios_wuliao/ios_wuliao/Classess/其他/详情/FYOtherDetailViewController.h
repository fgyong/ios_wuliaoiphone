//
//  FYOtherDetailViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYBaseViewController.h"

#import "FYBaseViewController.h"
#import "FYSaleHeaderView.h"
#import "FYSaleHeaderCell.h"
#import "FYAllocDetailCell.h"

@protocol FYOtherDetailViewControllerDelegate <NSObject>

@optional
-(void)shenpiStatusComplate;

@end

@interface FYOtherDetailViewController : FYBaseViewController

/**
 2 是调拨
 调拨和其他的详情都是用的这个
 */
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,copy) NSString * order_id;
@property (nonatomic,assign) BOOL needShenpi;


@property (nonatomic,weak) id<FYOtherDetailViewControllerDelegate> delegate;
@end
