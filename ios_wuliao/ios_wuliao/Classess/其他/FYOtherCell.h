//
//  FYOtherCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYOtherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bianhao;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *depLabel;
@property (weak, nonatomic) IBOutlet UILabel *riqi;
@property (weak, nonatomic) IBOutlet UILabel *zhiwuyuan;
@property (weak, nonatomic) IBOutlet UILabel *cangku;

@property (weak, nonatomic) IBOutlet UILabel *dizhi;


-(void)setDic:(NSDictionary *)info;
@end
