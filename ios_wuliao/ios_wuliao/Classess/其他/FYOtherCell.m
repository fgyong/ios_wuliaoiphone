//
//  FYOtherCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/4.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYOtherCell.h"

@implementation FYOtherCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDic:(NSDictionary *)info{
    self.titleLabel.text = [self getString:info[@"ccusname"]];
    self.depLabel.text = [self getString:info[@"cdepname"]];
    
    self.riqi.text = [[self getString:info[@"dDate"]] componentsSeparatedByString:@" "] [0];
    
    self.dizhi.text = [self getString:info[@"cDeliverUnit"]];
    
    self.cangku.text = [self getString:info[@"cwhname"]];
    
    self.zhiwuyuan.text = [self getString:info[@"cpersonname"]];

      self.bianhao.text =  [self getString: info[@"ccode"]];
    
    
    
}

@end
