//
//  FYOtherViewController.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYOtherViewController.h"
#import "FYOtherDetailViewController.h"
#import "FYSelectSecondVC.h"
#import "FYSelectViewController.h"
#import "FYSalesDetailViewController.h"

@interface FYOtherViewController ()<FYBaseTableViewController,FYOtherDetailViewControllerDelegate>

@end

@implementation FYOtherViewController
-(void)shenpiStatusComplate{
    [self.tableView freshHeader];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_tableView freshHeader];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDTitle:@"其他"];
    [self setMMDRightItemWithTitle:@"查询" action:@selector(select)];
    self.tableView =[[ FYBaseTableViewController alloc]initWithStyle:UITableViewStylePlain];
    self.tableView.sectionTitles = @[@"待审核其他出库单",@"已审核其他出库单"];
    self.tableView.title=@"";
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    rect.size.height = kScreenHeight - 64 - 49 - 64;
    self.tableView.view.frame = rect;
    
    [self.view addSubview:self.tableView.view];
    __weak FYOtherViewController *__weakSelf = self;
    [self.tableView setSelectIndex:^(NSIndexPath *indexpath,NSString * orderId) {
        NSLog(@"section:%ld row:%ld",indexpath.section,indexpath.row);
        FYOtherDetailViewController * Detail=[[FYOtherDetailViewController alloc]init];
        Detail.hidesBottomBarWhenPushed = YES;
        Detail.delegate = __weakSelf;
        Detail.order_id = orderId;
        Detail.needShenpi = !indexpath.section;
        
        [__weakSelf .navigationController pushViewController:Detail animated:YES];
        Detail.hidesBottomBarWhenPushed = NO;
    }];
    self.tableView.urlType = 0;
    self.tableView.parmas=[NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                          @"dPreDate":@"",
                                                                          @"cDepcode":@"",
                                                                          @"cInvcode":@""}];
    self.tableView.funcNameTop     = @"queryRdRecord09_NotApproved";
    self.tableView.funcNameHistory = @"queryRdRecord09_All";
    self.tableView.type = 1;
    [self.tableView freshHeader];
}
-(void)select{
    FYSelectViewController * fy =[[FYSelectViewController alloc]init];
    [fy successBlock:^(NSDictionary *result) {
        self.tableView.parmas = [NSMutableDictionary dictionaryWithDictionary:@{@"cCusCode":@"",
                                                                                @"dPreDate":@"",
                                                                                @"cDepcode":@"",
                                                                                @"cInvcode":@""}];
        for (NSString * item in result) {
            [self.tableView.parmas setObject:result[item] forKey:item];
        }
        [self.tableView freshHeader];
    }];
    fy.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fy animated:YES];
    fy.hidesBottomBarWhenPushed = NO;
}

 


@end
