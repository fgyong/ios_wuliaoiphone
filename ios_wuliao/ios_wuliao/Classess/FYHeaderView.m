//
//  FYHeaderView.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/28.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYHeaderView.h"

@interface FYHeaderView ()
@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel *NumberLabel;

@property (nonatomic,strong) UIImageView *arrow;

@property (nonatomic,copy) clickBlock block;

@property (nonatomic,strong) FYLable * line;
@property (nonatomic,strong) UIImageView * imageV;

@end

@implementation FYHeaderView
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    return self;
}

-(void)title:(NSString *)title
                   imageName:(NSString *)name
                      number:(NSString *)number
                  openStatus:(BOOL)isOpen{
    
        self.frame = CGRectMake(0, 0, kScreenWidth, 35);
    if (_imageV == nil) {
        _imageV=[[UIImageView alloc]initWithImage:[UIImage imageNamed:name]];
        [self.contentView addSubview:_imageV];
        _imageV.frame = CGRectMake(13, 7, 20, 20);
    }
    _imageV.image=[UIImage imageNamed:name];

    if (self.titleLabel == nil) {
        self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(34, 9, 150, 17)];
        self.titleLabel.text = title;
        self.titleLabel.font = Font(17);
        self.titleLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.titleLabel];
    }

        
    if (self.NumberLabel == nil) {
        self.NumberLabel=[[UILabel alloc]initWithFrame:CGRectMake(kScreenWidth - 75  ,9, 17, 17)];
       
        self.NumberLabel.font = Font(10);
        self.NumberLabel.textColor = [UIColor whiteColor];
        self.NumberLabel.clipsToBounds = YES;
        self.NumberLabel.layer.cornerRadius = 7.5;
        self.NumberLabel.textAlignment = NSTextAlignmentCenter;
        self.NumberLabel.adjustsFontSizeToFitWidth = YES;
        self.NumberLabel.backgroundColor = COLOR_Subject_Color;
        [self.contentView addSubview:self.NumberLabel];
    }
     self.NumberLabel.text = number;

        
    if (self.arrow == nil) {
        self.arrow =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrowRight"]];
        [self.contentView addSubview:self.arrow];
    }
        self.arrow.frame = CGRectMake(self.NumberLabel.mj_origin.x + 27,  9.5, 8, 14);
        
        UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click:)];
        [self.contentView addGestureRecognizer:tap];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setOpenStatus:isOpen];
        [self addSubview:self.line];
   
}
-(void)setNumber:(NSInteger)number{
    self.NumberLabel.text = [NSString stringWithFormat:@"%ld",number];
}
-(void)setOpenStatus:(BOOL)isOpen{
    self.open = isOpen;

    [UIView animateWithDuration:0.25 animations:^{
        if (isOpen) {
            self.arrow.image =[UIImage imageNamed:@"arrowRight"];
            self.arrow.frame = CGRectMake(self.NumberLabel.mj_origin.x + 27,  9.5, 8, 14);
        } else {
             self.arrow.image =[UIImage imageNamed:@"arrowDown"];
            self.arrow.frame = CGRectMake(self.NumberLabel.mj_origin.x + 27-3,  12.5, 14, 8);
        }
    }];
}
-(void)title:(NSString *)title
        open:(BOOL)isOpen{
    self.titleLabel.text = title;
    [self setOpenStatus:isOpen];
}
-(FYLable *)line{
    if (_line == nil) {
        _line =[FYLable lineWithFrame:CGRectMake(13, 34, kScreenWidth - 26-26, 0.5)];
    }
    return _line;
}
-(void)setClickBlock:(clickBlock)block{
    if (block) {
        self.block= block;
    }
}
-(void)click:(UITapGestureRecognizer *)tap{
 

    if (self.block) {
        self.block(self.tag);
 
    }
}

@end
