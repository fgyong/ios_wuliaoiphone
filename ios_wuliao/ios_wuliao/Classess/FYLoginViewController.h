//
//  FYLoginViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/28.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYDatePickerView.h"
#import "JPUSHService.h"


@interface FYLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *riqiField;//选择日期

- (IBAction)changeAccountType:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *zhangtaoTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UITextField *caozuoyuanTextField;
- (IBAction)login:(id)sender;
- (IBAction)changePwd:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconTopLayout;

@end
