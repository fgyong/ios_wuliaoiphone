//
//  FYSelectSecondVC.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSelectSecondVC.h"

@interface FYSelectSecondVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    NSMutableArray * _dataSource;
}
@end

@implementation FYSelectSecondVC
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMMDBackItem:NO];
    [self setMMDTitle:@"查询"];
 
   // [self setMMDItemTitle:@"条件筛选" action:@selector(showPicker) isLeftItem:NO withtextcolor:COLORRGB(68, 68, 68)];
    [self UI];
    if (self.type == 5) {
        
    }else {
        [self getDataFromServer];
    }
}
-(void)UI{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.searchBar.mas_bottom).offset(10);
    }];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FYSelectSecondTableViewCell" bundle:nil] forCellReuseIdentifier:@"FYSelectSecondTableViewCell"];
    
    _dataSource=[NSMutableArray array];
}
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [self done];
    }
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    if (searchBar.text.length == 0) {
        [self done];
    }
    return YES;
}
-(void)getDataFromServer{
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (self.searchBar.text.length) {
        [dic setObject:self.searchBar.text forKey:@"condition"];
    } else {
        [dic setObject:@"" forKey:@"condition"];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpRequest requestPostWithParm:dic
                              method:self.funcName
                         startHander:^(NSProgress *downloadProgress) {
        
    } success:^(NSDictionary *result) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if ([result[@"returnCode"] integerValue] == 10000) {
            NSLog(@"%@",result.description);
            NSArray * array =  result[@"aryList"];
            [_dataSource removeAllObjects];
            if (array.count) {
                [_dataSource addObjectsFromArray:result[@"aryList"]];
                [self.tableView reloadData];
            } else {
                [_dataSource removeAllObjects];
                [self.tableView reloadData];
            }
        } else {
            showFadeOutView(result[@"errDesc"], NO, 2);
        }
    } fialHander:^(NSString *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } needPwd:YES];
}
-(void)getDataFromServer2{
    NSMutableDictionary * dic=[NSMutableDictionary dictionary];
    if (self.searchBar.text.length) {
        [dic setObject:self.searchBar.text forKey:@"cCondition"];
    } else {
        [dic setObject:@"" forKey:@"cCondition"];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpRequest requestPostWithParm:dic
                              method:self.funcName
                         startHander:^(NSProgress *downloadProgress) {
                             
                         } success:^(NSDictionary *result) {
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             if ([result[@"returnCode"] integerValue] == 10000) {
                                 NSLog(@"%@",result.description);
                                 NSArray * array =  result[@"aryList"];
                                 [_dataSource removeAllObjects];
                                 if (array.count) {
                                     [_dataSource addObjectsFromArray:result[@"aryList"]];
                                     [self.tableView reloadData];
                                 } else {
                                     [_dataSource removeAllObjects];
                                     [self.tableView reloadData];
                                 }
                             } else {
                                 showFadeOutView(result[@"errDesc"], NO, 2);
                             }
                         } fialHander:^(NSString *error) {
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                         } needPwd:YES];
}

#pragma mark 搜索完成
-(void)done{
    [self.searchBar endEditing:YES];
    if (self.type == 5) {
        [self getDataFromServer2];
    }else {
        [self getDataFromServer];
    }
}

#pragma mark -UITabelView DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FYSelectSecondTableViewCell * cell =[tableView dequeueReusableCellWithIdentifier:@"FYSelectSecondTableViewCell" forIndexPath:indexPath];
    if (self.type == 5) {
        cell.titleContentLabel.text = [self stringWithRow2:indexPath.row];
    }else {
        cell.titleContentLabel.text = [self stringWithRow:indexPath.row];
    }
    
    return  cell;
}
-(NSString *)stringWithRow:(NSInteger)row{
    NSDictionary * info = _dataSource[row];
    NSString * str = @"";
    switch (_type) {
        case 0: str = [NSString stringWithFormat:@"%@ %@",info[@"cCode"],info[@"cName"]]; break;
        case 2: str = [NSString stringWithFormat:@"%@ %@ %@",info[@"cCode"],info[@"cName"],info[@"cInvStd"]]; break;
        case 3: str = [NSString stringWithFormat:@"%@ %@",info[@"cCode"],info[@"cName"]]; break;
        default:
            break;
    }
    return str;
}
#pragma mark -历史记录数据处理
-(NSString *)stringWithRow2:(NSInteger)row{
    NSDictionary * info = _dataSource[row];
    NSArray * array = @[@"01",
                        @"EX24",
                        @"0302",
                        @"0304"];
    NSString * type = [info objectForKey:@"cVouchType"];
    
    NSInteger index = [array indexOfObject:type];
    NSArray * titles =@[@"销售发货单",@"出口销货单",@"其他出库单",@"调拨单"];
    NSString * title = @"";
    if (index < 4) {
        title = titles[index];
    }
    NSString * str = str = [NSString stringWithFormat:@"%@：%@",NSLocalizedString(title, nil),info[@"cCode"]];
    return str;
}
#pragma mark -UITabelView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary * info = _dataSource[indexPath.row];
    NSString * order = info[@"cID"];
    if (self.type == 5) {
        NSArray * array = @[@"01",
                            @"EX24",
                            @"0302",
                            @"0304"];
        NSString * type = [info objectForKey:@"cVouchType"];
        
        switch ([array indexOfObject:type]) {
            case 0:[self pushSaleWithInfo:order];break;
            case 1:[self pushExitWithInfo:order];break;
            case 2:[self pushOtherWithDIc:order];break;
            case 3:[self pushAllocation:order];break;
            default:
                break;
        }
        return;
    }
    if (self.success) {
        self.success(info);
    }
    [self __popViewControllerAnimated];
}
-(void)pushSaleWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc =[[FYSalesDetailViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.order_id = orderID;
    //vc.delegate = self;
    vc.needShenpi = NO;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushOtherWithDIc:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.order_id = orderid;
    vc.needShenpi = NO;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushAllocation:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.type = 2;
    vc.needShenpi = NO;
    vc.order_id = orderid;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
-(void)pushExitWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc=[[FYSalesDetailViewController alloc]init];
    vc.type = 1;
    vc.hidesBottomBarWhenPushed = YES;
  //  vc.titleStr = @"出口审核";
    vc.order_id = orderID;
    vc.needShenpi = NO;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = NO;
}
- (void)__popViewControllerAnimated{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)setselectSeccess:(NetSuccessHander)success
{
    if (success) {
        self.success = success;
    }
}
@end
