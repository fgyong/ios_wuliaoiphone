//
//  FYSelectSecondTableViewCell.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSelectSecondTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleContentLabel;

@end
