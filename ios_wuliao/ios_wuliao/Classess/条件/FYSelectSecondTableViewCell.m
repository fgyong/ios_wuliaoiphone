//
//  FYSelectSecondTableViewCell.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYSelectSecondTableViewCell.h"

@implementation FYSelectSecondTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
