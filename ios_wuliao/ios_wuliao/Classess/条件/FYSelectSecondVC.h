//
//  FYSelectSecondVC.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYSelectSecondTableViewCell.h"

@interface FYSelectSecondVC : UIViewController
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;



@property (nonatomic,copy) NSString * funcName;
@property (strong, nonatomic)  UITableView *tableView;

/**
 5是 首页历史单据查询    
 */
@property (nonatomic,assign) NSInteger type;

@property (nonatomic,strong) NetSuccessHander success;

-(void)setselectSeccess:(NetSuccessHander)success;
@end
