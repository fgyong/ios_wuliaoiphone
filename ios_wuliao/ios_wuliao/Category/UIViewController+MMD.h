//
//  UIViewController+MMD.h
//  nan-ios
//
//  Created by mingshi on 14-8-18.
//  Copyright (c) 2014年 meimeidou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIControl+FY.h"
#import "MDNavigationController.h"


/*默认字体 frame 等 数字定义区*/
//int  const FYDefaultNavigationItemFontSize = 13; /*nav 默认的字体大小*/


////////////////////////////////////////////////////////////////////////////////

@interface UIViewController(MMD)

-(void)setMMDTitle:(NSString *)title;
 
- (void)setMMDTitle:(NSString*)title withBackgroundColor:(UIColor *)color titleColor:(UIColor *)titleColor;
- (void)setMMDTitle:(NSString*)title withcolor:(UIColor*)color;
- (void)setMMDTitle:(NSString *)title titleColor:(UIColor *)textColor action:(SEL)action;

- (void)setMMDBackItem:(BOOL)isFrosted;

- (void)setMMDBackItemWithWhiteBar:(BOOL)isFrosted withImage:(UIImage *)image;

- (void)setMMDDismissBackItem;

- (void)setMMDRightItemWithTitle:(NSString *)title action:(SEL)action;
- (void)setMMDLeftItemWithTitle:(NSString *)title action:(SEL)action;
- (void)setMMDItemWithPinkTitle:(NSString*)title action:(SEL)action isLeftItem:(BOOL)isLeft;
- (void)setMMDItemWithTitle:(NSString*)title titleColor:(UIColor *)titleColor action:(SEL)action isLeftItem:(BOOL)isLeft;

- (void)setMMDCancelItem;

- (void)setMMDRightImageItemWithImgName:(NSString *)imgName action:(SEL)action;
- (void)setMMDItemWithImageName:(NSString *)imgName action:(SEL)action isLeftItem:(BOOL)isLeft;

- (void)addGeoCityObservation;
- (void)removeGeoCityObservation;

- (void)showNavigationBar;
- (void)showNavigationBarWhite;

- (void)setMMDItemWithTitleAndImage:(NSString*)title image:(NSString *)imageName action:(SEL)action isLeftItem:(BOOL)isLeft;

- (void)setMMDNewItemWithTitleAndImage:(NSString*)title image:(NSString *)imageName action:(SEL)action isLeftItem:(BOOL)isLeft;

- (UIViewController*)getViewControllerFromStackIfCan:(Class)classType;
//添加settitleWithcolor

- (void)setMMDItemTitle:(NSString*)title action:(SEL)action isLeftItem:(BOOL)isLeft withtextcolor:(UIColor*)color;

- (void)setMMDTitle:(NSString *)title image:(NSString *)image action:(SEL)action;

- (void)setMMDItemWithTitle:(NSString*)title target:(id)target action:(SEL)action isLeftItem:(BOOL)isLeft;

- (void)setMutiItemsWithTitleAndImage:(NSArray *)titles images:(NSArray *)images action:(SEL)action isLeftItem:(BOOL)isLeft;

- (void)setTitleWithSubTitle:(NSString *)title subTitle:(NSString *)subTitle;

- (void)setMMDItemTitle:(NSString*)title target:(id)target action:(SEL)action isLeftItem:(BOOL)isLeft withtextcolor:(UIColor*)color;

-(void)setMMDBackgroundColor:(UIColor *)backgroundcolor;
- (void)__popViewControllerAnimated;

- (void)setMMDTitle:(NSString *)title fontSize:(UIFont *)font withBackgroundColor:(UIColor *)color titleColor:(UIColor *)titleColor;
@end
