//
//  UIViewController+MMD.m
//  nan-ios
//
//  Created by mingshi on 14-8-18.
//  Copyright (c) 2014年 meimeidou. All rights reserved.
//

#import "UIViewController+MMD.h"


@implementation UIViewController(MMD)
#pragma mark - Set controller common navigation item

//新添加 有title并且有倒三角的 mingshi 2014-06-03
- (void)setMMDItemWithTitleAndImage:(NSString*)title image:(NSString *)imageName action:(SEL)action isLeftItem:(BOOL)isLeft
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    customBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    if ([title length]) {
     [customBtn setTitle:title forState:UIControlStateNormal];
    }
 
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    UIColor *pinkColor = [UIColor whiteColor];//[UIColor colorWithRed:255/255.0 green:102/255.0 blue:153/255.0 alpha:1];
    UIColor *pinkSelectedColor = [UIColor whiteColor];//[UIColor colorWithRed:217/255.0 green:87/255.0 blue:130/255.0 alpha:1];
    [customBtn setTitleColor:pinkColor forState:UIControlStateNormal];
    [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : customBtn.titleLabel.font} context:nil].size;
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    UIImage *_image = [UIImage imageNamed:imageName];
    UIImageView *arrowImage = [[UIImageView alloc]init];
    if (IS_OS_7_OR_LATER) {
        arrowImage.frame = CGRectMake(titleSize.width + 8, (customBtn.frame.size.height - _image.size.height) / 2, _image.size.width, _image.size.height);
    } else {
        arrowImage.frame = CGRectMake(titleSize.width + 8 + 8, (customBtn.frame.size.height - _image.size.height) / 2, _image.size.width, _image.size.height);
    }
    arrowImage.image = _image;
    
    [customBtn addSubview:arrowImage];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}
//end
-(void)setMMDBackItemWithbackTitle:(NSString *)title{
    
}
//加多个items
- (void)setMutiItemsWithTitleAndImage:(NSArray *)titles images:(NSArray *)images action:(SEL)action isLeftItem:(BOOL)isLeft
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
   
    CGFloat customBtnWidth = 44 * [images count] + ([images count] - 1) * 1;
    CGFloat customBtnHeight = 20 + 16;
    
    customBtn.frame = CGRectMake(0, 0, customBtnWidth, customBtnHeight);
    
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }

    for (int i = 0; i < [images count]; i++) {
        NSString *str = (NSString *)[images objectAtIndex:i];
        UIImage *img = [UIImage imageNamed:str];
        
        CGFloat x = i * 44 + i * 1;
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, 44, 36)];
        btn.backgroundColor = [UIColor clearColor];
        [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
        [customBtn addSubview:btn];
       
        if (!isLeft) {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
            } else {
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
            }
        }else
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
            } else {
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
            }
        }

        
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(12, 0, 20, 20)];
        image.image = img;
        [btn addSubview:image];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 44, 11)];
        label.font = [UIFont systemFontOfSize:10];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [titles objectAtIndex:i];
        [btn addSubview:label];
       
        btn.tag = i + 1;
        
        if (i < [images count] - 1) {
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(btn.frame.origin.x + btn.frame.size.width, 5, 0.5, 26)];
            line.backgroundColor = [UIColor whiteColor];
            [customBtn addSubview:line];
        }
    }
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}
//end

//新项目，城市选择，图标在左边了
- (void)setMMDNewItemWithTitleAndImage:(NSString*)title image:(NSString *)imageName action:(SEL)action isLeftItem:(BOOL)isLeft
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    customBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    //[customBtn setTitle:title forState:UIControlStateNormal];
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    UIColor *pinkColor = [UIColor whiteColor];//[UIColor colorWithRed:255/255.0 green:102/255.0 blue:153/255.0 alpha:1];
    UIColor *pinkSelectedColor = [UIColor whiteColor];//[UIColor colorWithRed:217/255.0 green:87/255.0 blue:130/255.0 alpha:1];
    [customBtn setTitleColor:pinkColor forState:UIControlStateNormal];
    [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 30, 31);
    
    UIImage *_image = [UIImage imageNamed:imageName];
    UIImageView *arrowImage = [[UIImageView alloc]init];
    
    arrowImage.frame = CGRectMake(0, (customBtn.frame.size.height - _image.size.height) / 2, _image.size.width, _image.size.height);
    arrowImage.image = _image;
    
    [customBtn addSubview:arrowImage];
    
    UILabel *btnLabel = [[UILabel alloc]init];
    if (IS_OS_7_OR_LATER) {
        btnLabel = [[UILabel alloc]initWithFrame:CGRectMake(arrowImage.frame.size.width + 8, (customBtn.frame.size.height - 16) / 2.0, titleSize.width + 5, titleSize.height)];
    } else {
        btnLabel = [[UILabel alloc]initWithFrame:CGRectMake(arrowImage.frame.size.width + 8, (customBtn.frame.size.height - 16) / 2.0, titleSize.width + 5, titleSize.height)];
    }
    btnLabel.text = title;
    btnLabel.backgroundColor = [UIColor clearColor];
    btnLabel.textColor = [UIColor whiteColor];
    [customBtn addSubview:btnLabel];
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

//end




- (void)setMMDItemWithTitle:(NSString *)title titleColor:(UIColor *)titleColor action:(SEL)action isLeftItem:(BOOL)isLeft
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    customBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [customBtn setTitle:title forState:UIControlStateNormal];
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, -7)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -5, 0, -7)];
        }
    }else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    //UIColor *pinkColor = [UIColor whiteColor];//[UIColor colorWithRed:255/255.0 green:102/255.0 blue:153/255.0 alpha:1];
    UIColor *pinkSelectedColor = [UIColor whiteColor];//[UIColor colorWithRed:217/255.0 green:87/255.0 blue:130/255.0 alpha:1];
    [customBtn setTitleColor:titleColor forState:UIControlStateNormal];
    [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

#pragma mark - 添加target
- (void)setMMDItemWithTitle:(NSString*)title target:(id)target action:(SEL)action isLeftItem:(BOOL)isLeft
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    customBtn.titleLabel.font = Font(14);
    [customBtn setTitle:title forState:UIControlStateNormal];
      if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, -7)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -5, 0, -7)];
        }
    }else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    UIColor *pinkColor = [UIColor whiteColor];//[UIColor colorWithRed:255/255.0 green:102/255.0 blue:153/255.0 alpha:1];
    UIColor *pinkSelectedColor = [UIColor whiteColor];//[UIColor colorWithRed:217/255.0 green:87/255.0 blue:130/255.0 alpha:1];
    [customBtn setTitleColor:pinkColor forState:UIControlStateNormal];
    [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

- (void)setMMDItemTitle:(NSString*)title action:(SEL)action isLeftItem:(BOOL)isLeft withtextcolor:(UIColor*)color
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    customBtn.acceptEventInterval = 2;
    customBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [customBtn setTitle:title forState:UIControlStateNormal];
    [customBtn setTitleColor:color forState:UIControlStateNormal];
    
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }
    else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
  //  UIColor *pinkColor = color;
  //  UIColor *pinkSelectedColor = color;
   // [customBtn setTitleColor:pinkColor forState:UIControlStateNormal];
   // [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

- (void)setMMDItemTitle:(NSString*)title target:(id)target action:(SEL)action isLeftItem:(BOOL)isLeft withtextcolor:(UIColor*)color
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    customBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [customBtn setTitle:title forState:UIControlStateNormal];
    
    
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }
    else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    UIColor *pinkColor = color;
    UIColor *pinkSelectedColor = color;
    [customBtn setTitleColor:pinkColor forState:UIControlStateNormal];
    [customBtn setTitleColor:pinkSelectedColor forState:UIControlStateHighlighted];
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}



- (void)setMMDItemWithPinkTitle:(NSString*)title action:(SEL)action isLeftItem:(BOOL)isLeft {
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    customBtn.titleLabel.font = FONTSYSTEMSIZE(16);
    [customBtn setTitle:title forState:UIControlStateNormal];
    
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }
    else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(100, 31)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(100, 31) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    customBtn.frame = CGRectMake(0, 0, titleSize.width + 20, 31);
    
    [customBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [customBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

- (void)setMMDRightItemWithTitle:(NSString *)title action:(SEL)action{
    [self setMMDItemWithTitle:NSLocalizedString(title, @"") titleColor:COLORRGB(68, 68, 68) action:action isLeftItem:NO];
}
- (void)setMMDLeftItemWithTitle:(NSString *)title action:(SEL)action{
     [self setMMDItemWithTitle:title titleColor:COLOR_Subject_Color action:action isLeftItem:YES];
}

- (void)setMMDItemWithImageName:(NSString *)imgName action:(SEL)action isLeftItem:(BOOL)isLeft {
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    if (!isLeft) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, 17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
    }
    else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(4, -17, 0, 0)];
        } else {
            [customBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        }
    }
    
    customBtn.frame = CGRectMake(0, 0, 33, 44);
    [customBtn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
   // customBtn.backgroundColor =[UIColor redColor];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    if (!isLeft) {
        self.navigationItem.rightBarButtonItem = item;
    }
    else {
        self.navigationItem.leftBarButtonItem = item;
    }
}

- (void)setMMDRightImageItemWithImgName:(NSString *)imgName action:(SEL)action{
    [self setMMDItemWithImageName:imgName action:action isLeftItem:NO];
}

- (void)receivedCityNotification {
}

- (void)showNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
  //  self.navigationController.navigationBar.customBarStyle = MYNaigationBarStylePink;
    self.navigationController.navigationBarHidden = NO;
}

- (void)showNavigationBarWhite
{
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
  //  self.navigationController.navigationBar.customBarStyle = MYNaigationBarStyleWhite;
    self.navigationController.navigationBarHidden = NO;
}

- (UIViewController*)getViewControllerFromStackIfCan:(Class)classType {
    NSArray *arr = self.navigationController.viewControllers;
    UIViewController *popViewCtr = NULL;
    
    for (id viewCtr in arr) {
        if ([viewCtr isKindOfClass:classType]) {
            popViewCtr = viewCtr;
            break;
        }
    }
    return popViewCtr;
}

#pragma mark -
#pragma mark 设置navigation返回按钮

// pop的返回按钮
- (void)__popViewControllerAnimated
{
    NSLog(@"pop");
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setMMDBackItem:(BOOL)isFrosted {
    [self setMMDBackItemWithWhiteBar:isFrosted withImage:nil];
}
- (void)setMMDBackItemWithWhiteBar:(BOOL)isFrosted withImage:(UIImage *)image
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self formatMMDBackItem:backBtn withImage:image];
   // [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backBtn.titleLabel.font = Font(14);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    if (!isFrosted) {
        [backBtn addTarget:self
                action:@selector(__popViewControllerAnimated)
      forControlEvents:UIControlEventTouchUpInside];
    } else {
        [backBtn addTarget:self
                    action:@selector(hideMenu)
          forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (void)hideMenu
{
    MDNavigationController *nav = (MDNavigationController *)self.navigationController;
    [nav hideMenu];
}

// dismiss的返回按钮
- (void)__dismissModalViewControllerAnimated
{
    [self dismissViewControllerAnimated:YES completion:^{ }];
}
- (void)setMMDCancelItem { [self setMMDDismissBackItem]; }
- (void)setMMDDismissBackItem
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self formatMMDBackItem:backBtn withImage:nil];
    
    [backBtn addTarget:self
                action:@selector(__dismissModalViewControllerAnimated)
      forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
}


- (void)formatMMDBackItem:(UIButton *)btn withImage:(UIImage *)image
{
    UIImageView * imageV =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"return"]];
    imageV.frame = CGRectMake(0, 12, 8, 16);
//    if (image) {
//        [btn setImage:image forState:UIControlStateNormal];
//    }else {
//      [btn setImage:[UIImage imageNamed:@"return"]forState:UIControlStateNormal];
//    }
    [btn addSubview:imageV];
    btn.frame = CGRectMake(0, 0, 50, 40);
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 11, 0, 0)];
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//        [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -17, 0, 0)];
//    } else {
//        [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
//    }
}

#pragma mark -
#pragma mark 设置navigation标题

//添加settitleWithcolor
- (void)setMMDTitle:(NSString*)title withcolor:(UIColor*)color
{
    if (title.length == 0) {
        return;
    }
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 45)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:19.0];
    titleView.textColor =color;
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = title;
    //  [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
}
-(void)setMMDTitle:(NSString *)title{
    [self setMMDTitle:NSLocalizedString(title, @"") withBackgroundColor:COLORRGB(247,247,247) titleColor:COLORRGB(68, 68, 68)];
}
 
-(void)setMMDBackgroundColor:(UIColor *)backgroundcolor {
    self.navigationController.navigationBar.barTintColor = backgroundcolor;
}
- (void)setMMDTitle:(NSString *)title withBackgroundColor:(UIColor *)color titleColor:(UIColor *)titleColor
{
    [self setMMDBackgroundColor:color];
    if (title.length == 0) {
        return;
    }
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 45)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:18.0];
    if (titleColor) {
        titleView.textColor = titleColor ;
    }else {
      titleView.textColor = [UIColor whiteColor];
    }
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = NSLocalizedString(title, nil);
    self.navigationItem.titleView = titleView;
    titleView.center = CGPointMake(titleView.superview.center.x, titleView.superview.center.y);
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
}
- (void)setMMDTitle:(NSString *)title fontSize:(UIFont *)font withBackgroundColor:(UIColor *)color titleColor:(UIColor *)titleColor
{
    [self setMMDBackgroundColor:color];
    if (title.length == 0) {
        return;
    }
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 45)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = font;
    if (titleColor) {
        titleView.textColor = titleColor ;
    }else {
        titleView.textColor = [UIColor whiteColor];
    }
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = NSLocalizedString(title, nil);
    self.navigationItem.titleView = titleView;
    titleView.center = CGPointMake(titleView.superview.center.x, titleView.superview.center.y);
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
}
//新加的包含subtitle的
- (void)setTitleWithSubTitle:(NSString *)title subTitle:(NSString *)subTitle
{
    if (title.length == 0 && subTitle.length == 0) {
        return;
    }
    
    UIView *titleView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width - 250, 45)];
    titleView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleView;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 6, titleView.frame.size.width, 20)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:19];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = title;
    [titleView addSubview:titleLabel];
    
    UILabel *subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, titleLabel.frame.size.height + titleLabel.frame.origin.y, titleView.frame.size.width, 12)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.text = subTitle;
    [titleView addSubview:subTitleLabel];
    
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
}
//end

//新加的navigation上带下拉图片的
- (void)setMMDTitle:(NSString *)title image:(NSString *)image action:(SEL)action
{
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    customBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    customBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [customBtn setTitle:title forState:UIControlStateNormal];
    [customBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[customBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(200, 45)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(200, 45) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    //customBtn.frame = CGRectMake(0, 0, titleSize.width + 20 + 11, 45);
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = customBtn;
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(titleSize.width + 20, 20, 11, 6)];
    arrow.image = [UIImage imageNamed:image];
    customBtn.frame = CGRectMake((kScreenWidth - titleSize.width - 20 - arrow.frame.size.width) / 2, 0, titleSize.width + 20 + 11, 44);
    [customBtn addSubview:arrow];
    
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }

}

- (void)setMMDTitle:(NSString *)title titleColor:(UIColor *)textColor action:(SEL)action
{
    
    UIButton *customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    customBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    customBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [customBtn setTitle:title forState:UIControlStateNormal];
    [customBtn setTitleColor:textColor forState:UIControlStateNormal];
  //  [customBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    CGSize titleSize = [title sizeWithFont:customBtn.titleLabel.font constrainedToSize:CGSizeMake(200, 45)];
    
    CGSize titleSize = [title boundingRectWithSize:CGSizeMake(200, 45) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:customBtn.titleLabel.font} context:nil].size;
    
    //customBtn.frame = CGRectMake(0, 0, titleSize.width + 20 + 11, 45);
    [customBtn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = customBtn;
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(titleSize.width + 20, 20, 11, 6)];
    arrow.image = [UIImage imageNamed:@"category_arrow"];
    customBtn.frame = CGRectMake((kScreenWidth - titleSize.width - 20 - arrow.frame.size.width) / 2, 0, titleSize.width + 20 + 11, 44);
    [customBtn addSubview:arrow];
    
    if (IS_OS_7_OR_LATER) {
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    } else {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
}

- (void)addGeoCityObservation {
}

- (void)removeGeoCityObservation {
    
}






@end
