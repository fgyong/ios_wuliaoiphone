//
//  NSObject+YF.m
//  ios_wuliao
//
//  Created by Charlie on 2017/6/15.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "NSObject+YF.h"

@implementation NSObject (YF)
-(NSString *)getString:(NSString *)str{
    if ([str isKindOfClass:[NSNull class]]) {
        return @"";
    }
    if ([str length]) {
        return str;
    }
    return str;
}
-(NSString * )monery:(NSString *)monery WithForamt:(NSString *)format{
    double Monery2= [monery floatValue];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:format];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:Monery2]];
    return formattedNumberString;
}
-(NSString * )monery:(NSString *)monery {
    double Monery= [monery floatValue];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###,###,##0.00"];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:Monery]];
    return formattedNumberString;
}
+ (NSString*)getPreferredLanguage
{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    return preferredLang;
}
+(BOOL)isJapanceLangue{
    if ([[self getPreferredLanguage] hasPrefix:@"Ja"]) {
        return YES;
    }
    return NO;
}
@end
