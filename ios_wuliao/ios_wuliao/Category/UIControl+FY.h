//
//  UIControl+FY.h
//  xiaoqu-ios
//
//  Created by Charlie on 15/10/21.
//  Copyright © 2015年 meimeidou. All rights reserved.
// 给按钮添加 每次响应时间，杜绝一直无限次点击

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UIControl (FY)
@property (nonatomic, assign) NSTimeInterval acceptEventInterval;
@property (nonatomic) BOOL ignoreEvent;


@end
