//
//  AppDelegate.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/22.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYExitViewController.h"
#import "FYAllocationViewController.h"
#import "FYOtherViewController.h"
#import "FYSaleViewController.h"
#import "FYHomeViewController.h"
#import "FYSalesDetailViewController.h"
#import "FYOtherDetailViewController.h"
#import "FYKeyBoardManger.h"
#import "MDNavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic,strong) NSDictionary * dic;//记录冷启动 是否有推送
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,strong) UITabBarController * tabbar;
-(void )allocVC;
-(void)changeLogin;
-(UIViewController *)visiableVC;
-(void)resetTags;
@end

