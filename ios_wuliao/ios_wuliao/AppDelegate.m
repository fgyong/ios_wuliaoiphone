//
//  AppDelegate.m
//  ios_wuliao
//
//  Created by Charlie on 2017/5/22.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "AppDelegate.h"
#import "JPUSHService.h"
#import <AdSupport/AdSupport.h>
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#import "FYLoginViewController.h"
#import "FYKeyBoardManger.h"


static NSString *appKey = @"d4e47da8c981c27594ca3a27";//测试
static NSString *channel = @"app store";
static BOOL isProduction = FALSE;
@interface AppDelegate ()<JPUSHRegisterDelegate,UINavigationControllerDelegate>

@property (nonatomic,assign) BOOL bacomeActiveFromBack;
@end

@implementation AppDelegate

-(void )allocVC{
    FYSaleViewController * exit=[[FYSaleViewController alloc]initWithNibName:@"FYSaleViewController" bundle:nil];
    MDNavigationController * na=[[MDNavigationController alloc]initWithRootViewController:exit];
    na.tabBarItem.title = NSLocalizedString(@"销售", nil);
    na.tabBarItem.image = [UIImage imageNamed:@"salesPage"];
    na.tabBarItem.selectedImage = [[UIImage imageNamed:@"salesPageSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    na.delegate = self;
    [self.tabbar addChildViewController:na];
    
    
    FYExitViewController * sale=[[FYExitViewController alloc]initWithNibName:@"FYExitViewController" bundle:nil];
    MDNavigationController * naSale=[[MDNavigationController alloc]initWithRootViewController:sale];
    
    naSale.tabBarItem.title = NSLocalizedString(@"出口", nil);;
    naSale.tabBarItem.image = [UIImage imageNamed:@"exportPage"];
    naSale.tabBarItem.selectedImage = [[UIImage imageNamed:@"exportPageSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naSale.delegate = self;
    [self.tabbar addChildViewController:naSale];
    
    FYOtherViewController * other=[[FYOtherViewController alloc]initWithNibName:@"FYOtherViewController" bundle:nil];
    MDNavigationController * naOther=[[MDNavigationController alloc]initWithRootViewController:other];
    
    naOther.tabBarItem.title = NSLocalizedString(@"其他", nil);
    naOther.tabBarItem.image = [UIImage imageNamed:@"otherPage"];
    naOther.tabBarItem.selectedImage = [[UIImage imageNamed:@"otherPageSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naOther.delegate = self;
    [self.tabbar addChildViewController:naOther];
    
    FYAllocationViewController * all=[[FYAllocationViewController alloc]initWithNibName:@"FYAllocationViewController" bundle:nil];
    MDNavigationController * naAll=[[MDNavigationController alloc]initWithRootViewController:all];
    naAll.tabBarItem.title = NSLocalizedString(@"调拨", nil);
    naAll.tabBarItem.image = [UIImage imageNamed:@"transferPage"];
    naAll.tabBarItem.selectedImage = [[UIImage imageNamed:@"transferPageSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    naAll.delegate = self;
    [self.tabbar addChildViewController:naAll];
    
    FYHomeViewController * home=[[FYHomeViewController alloc]initWithNibName:@"FYHomeViewController" bundle:nil];
    MDNavigationController * nahome=[[MDNavigationController alloc]initWithRootViewController:home];
    nahome.tabBarItem.title = NSLocalizedString(@"主页", nil);
    nahome.tabBarItem.image = [UIImage imageNamed:@"homePage"];
    nahome.tabBarItem.selectedImage = [[UIImage imageNamed:@"homePageSelected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.tabbar addChildViewController:nahome];
    self.tabbar.selectedIndex = 4;
    nahome.delegate = self;
    self.window.rootViewController = self.tabbar;
    if ([self.dic count]) {
        [self handleUserInfo:self.dic];
        self.dic = nil;//删除 推送这一次的信息 
    }
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
         AppDelegate * lication = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        if (navigationController.viewControllers.count > 1) {
            lication.tabbar.tabBar.hidden = YES;
        }else {
            lication.tabbar.tabBar.hidden = NO;
        }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
 
//    if (self.bacomeActiveFromBack == NO) {
//         [[UserInfoCache share] clearCache];
//    } else {
//        self.bacomeActiveFromBack = NO;
//    }
     [[UserInfoCache share] clearCache];
 
    
    FYLoginViewController * login = [[FYLoginViewController alloc]initWithNibName:@"FYLoginViewController" bundle:nil];
    if ([[UserInfoCache share] userInfo]) {
        [self allocVC];
    } else if(login) {
        self.window.rootViewController = login;
    }
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    // 3.0.0及以后版本注册可以这样写，也可以继续用旧的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    if (DEBUG) {
        isProduction = NO;
    }else {
        isProduction = YES;
    }
        [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:advertisingId];
    
    //2.1.9版本新增获取registration id block接口。
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"registrationID获取成功：%@",registrationID);
            
        }
        else{
            NSLog(@"registrationID获取失败，code：%d",resCode);
        }
    }];
    
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        NSDictionary * dic= launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
        self.dic =[NSDictionary dictionaryWithDictionary:dic];//记录 启动时候的推送info
       // [self performSelector:@selector(handleUserInfo:) withObject:dic afterDelay:2];
    }
    
    // [self allocVC];
    [self.window makeKeyAndVisible];
    [[FYKeyBoardManger shareManger] setValible:YES];
    
    return YES;
}
- (void)handleUserInfo:(NSDictionary *)userInfo{
    NSString * str =[userInfo objectForKey:@"toraycontent"];
    NSArray * array = [str componentsSeparatedByString:@"#"];
    NSString * type = array[0];
    type =[type stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * order = array[1];
    NSArray * types = @[@"01",
                        @"EX24",
                        @"0302",
                        @"0304"];
    NSInteger index = [types indexOfObject:type];
    switch (index) {
        case 0:[self pushSaleWithInfo:order];break;
        case 1:[self pushExitWithInfo:order];break;
        case 2:[self pushOtherWithDIc:order];break;
        case 3:[self pushAllocation:order];break;
        default:
            break;
    }

}
-(void)changeLogin{
    FYLoginViewController * login=[[FYLoginViewController alloc]initWithNibName:@"FYLoginViewController" bundle:nil];
    self.window.rootViewController = login;
}
-(UIViewController *)visiableVC{
    if ([self.window.rootViewController isKindOfClass:[self.tabbar class]]) {
        UINavigationController * na = self.tabbar.viewControllers[self.tabbar.selectedIndex];
        return na;
    } else {
        return self.window.rootViewController;
    }
}
-(UITabBarController *)tabbar{
    if (_tabbar == nil) {
        _tabbar = [[UITabBarController alloc]init];
    }
    return _tabbar;
}

- (void)networkDidSetup:(NSNotification *)notification {
    NSLog(@"已连接");
}

- (void)networkDidClose:(NSNotification *)notification {
    NSLog(@"未连接");
}

- (void)networkDidRegister:(NSNotification *)notification {
    NSLog(@"已注册");
}

- (void)networkDidLogin:(NSNotification *)notification {
    NSLog(@"已登录");
    
    if ([JPUSHService registrationID]) {
        NSLog(@"get RegistrationID");
    }
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *content = [userInfo valueForKey:@"content"];
    NSLog(@"appdelegeate%@", content);
}
-(void)resetTags{
    [JPUSHService setTags:[NSSet setWithObjects:@"", nil]
                    alias:nil
         callbackSelector:@selector(tagsAliasCallback:tags:alias:)
                   object:self];
}
- (void)serviceError:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *error = [userInfo valueForKey:@"error"];
    NSLog(@"%@", error);
}
- (void)applicationWillResignActive:(UIApplication *)application {
    //    [APService stopLogPageView:@"aa"]; 前台到后台
    // Sent when the application is about to move from active to inactive state.
    // This can occur for certain types of temporary interruptions (such as an
    // incoming phone call or SMS message) or when the user quits the application
    // and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down
    // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate
    // timers, and store enough application state information to restore your
    // application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called
    // instead of applicationWillTerminate: when the user quits.
    
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    //self.bacomeActiveFromBack = YES;//从后台到前台
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the
    // application was inactive. If the application was previously in the
    // background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //[self resetTags];
    NSLog(@"我被杀死~~~");
}
- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    NSString *callbackString =
    [NSString stringWithFormat:@"%d, \ntags: %@, \nalias: %@\n", iResCode,
     [self logSet:tags], alias];
    NSLog(@"TagsAlias回调:%@", callbackString);
}


- (NSString *)logSet:(NSSet *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //NSLog(@"%@", [NSString stringWithFormat:@"Device Token: %@", deviceToken]);
    [JPUSHService registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
   // NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings {
}

// Called when your app has been activated by the user selecting an action from
// a local notification.
// A nil action identifier indicates the default action.
// You should call the completion handler as soon as you've finished handling
// the action.
- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification
  completionHandler:(void (^)())completionHandler {
}

// Called when your app has been activated by the user selecting an action from
// a remote notification.
// A nil action identifier indicates the default action.
// You should call the completion handler as soon as you've finished handling
// the action.
- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void (^)())completionHandler {
}
#endif

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"iOS6及以下系统，收到通知:%@", [self logDic:userInfo]);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"iOS7及以上系统，收到通知:%@", [self logDic:userInfo]);
    
    
    NSString *apnCount = userInfo[@"aps"][@"alert"];
    NSLog(@"iOS7及以上系统，收到通知:%@", apnCount);
    
    if ([[UIDevice currentDevice].systemVersion floatValue]<10.0 || application.applicationState>0) {
        
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#pragma mark- JPUSHRegisterDelegate
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    NSDictionary * userInfo = notification.request.content.userInfo;
    
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
      //  NSLog(@"iOS10 前台收到远程通知:%@", [self logDic:userInfo]);
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 前台收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
}

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    UNNotificationRequest *request = response.notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        NSLog(@"iOS10  :%@", [self logDic:userInfo]);
        self.tabbar.tabBar.hidden = YES;
        [self performSelector:@selector(handleUserInfo:) withObject:userInfo afterDelay:0];
//        NSString * str =[userInfo objectForKey:@"toraycontent"];
//        NSArray * array = [str componentsSeparatedByString:@"#"];
//        NSString * type = array[0];
//        type =[type stringByReplacingOccurrencesOfString:@" " withString:@""];
//        NSString * order = array[1];
//        NSArray * types = @[@"01",
//                            @"EX24",
//                            @"0302",
//                            @"0304"];
//        NSInteger index = [types indexOfObject:type];
//        switch (index) {
//            case 0:[self pushSaleWithInfo:order];break;
//            case 1:[self pushExitWithInfo:order];break;
//            case 2:[self pushOtherWithDIc:order];break;
//            case 3:[self pushAllocation:order];break;
//            default:
//                break;
//        }

//        NSLog(@"%@",array.description);
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 收到本地通知:{\nbody:%@，\ntitle:%@,\nsubtitle:%@,\nbadge：%@，\nsound：%@，\nuserInfo：%@\n}",body,title,subtitle,badge,sound,userInfo);
    }
    
    completionHandler();  // 系统要求执行这个方法
}
#endif
-(void)pushSaleWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc =[[FYSalesDetailViewController alloc]init];
    
    vc.order_id = orderID;
    vc.needShenpi = YES;
    UIViewController * vc2 = [[FYKeyBoardManger shareManger] visibleVC];
    if (vc2.navigationController.viewControllers.count > 1) {
        vc.hidesBottomBarWhenPushed = YES;
    } else {
        vc.hidesBottomBarWhenPushed = NO;
    }
    [vc2.navigationController pushViewController:vc animated:YES];

}
-(void)pushOtherWithDIc:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.order_id = orderid;
    vc.needShenpi = YES;
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController * vc2 = [[FYKeyBoardManger shareManger] visibleVC];
    if (vc2.navigationController.viewControllers.count > 1) {
        vc.hidesBottomBarWhenPushed = YES;
    } else {
        vc.hidesBottomBarWhenPushed = NO;
    }
    [vc2.navigationController pushViewController:vc animated:YES];
 
}
-(void)pushAllocation:(NSString *)orderid{
    FYOtherDetailViewController * vc =[[FYOtherDetailViewController alloc]init];
    vc.type = 2;
    vc.needShenpi = YES;
    vc.order_id = orderid;
    vc.hidesBottomBarWhenPushed = YES;
    UIViewController * vc2 = [[FYKeyBoardManger shareManger] visibleVC];
    if (vc2.navigationController.viewControllers.count > 1) {
        vc.hidesBottomBarWhenPushed = YES;
    } else {
        vc.hidesBottomBarWhenPushed = NO;
    }
    [vc2.navigationController pushViewController:vc animated:YES];
 
}
-(void)pushExitWithInfo:(NSString *)orderID{
    FYSalesDetailViewController * vc=[[FYSalesDetailViewController alloc]init];
    vc.type = 1;
    vc.hidesBottomBarWhenPushed = YES;
    vc.titleStr = @"出口审核";
    vc.order_id = orderID;
    vc.needShenpi = YES;
    UIViewController * vc2 = [[FYKeyBoardManger shareManger] visibleVC];
    if (vc2.navigationController.viewControllers.count > 1) {
        vc.hidesBottomBarWhenPushed = YES;
    } else {
        vc.hidesBottomBarWhenPushed = NO;
    }
    [vc2.navigationController pushViewController:vc animated:YES];
 
}
// log NSSet with UTF8
// if not ,log will be \Uxxx
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

@end
