//
//  UtilConst.h
//  BLTSZY
//
//  Created by Charlie on 2017/3/23.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
// 工程的宏 all in this。

#ifndef UtilConst_h
#define UtilConst_h
#define NSLog(format, ...) do {                                             \
fprintf(stderr, "<%s : %d> %s\n",                                           \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__,__FUNCTION__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "\n");                                               \
} while (0)

#if DEBUG
#define baseUrl @"https://app.toray.cn:5443/dlWebservice.asmx"
#else
#define baseUrl @"https://app.toray.cn:5443/dlWebservice.asmx"
#endif




#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_9_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IS_OS_10_OR_LATER   ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
//手机类型
#define kDevice_Is_iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kDevice_Is_iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

#define kDevice_Is_iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define FONTSYSTEMSIZE(a) [UIFont systemFontOfSize:fontSizefit(a)]
#define FONTBOLDSIZE(size) [UIFont boldSystemFontOfSize:fontSizefit(size)]
static inline int fontSizefit(int sizefont) {if(kDevice_Is_iPhone5)
{
    return sizefont;
}
else if (kDevice_Is_iPhone6)
{
    return sizefont+2;
}
else if (kDevice_Is_iPhone6Plus)
{\
    return sizefont+3;
}else
{
    return sizefont;
}
}



//设备硬件信息
#define IsRetinaScreen   ((int)([UIScreen mainScreen].scale) == 2)
#define kAppViewWidth  [[UIScreen mainScreen] applicationFrame].size.width
#define kAppViewHeight [[UIScreen mainScreen] applicationFrame].size.height
#define kScreenWidth [[UIScreen mainScreen] bounds].size.width
#define kScreenHeight [[UIScreen mainScreen] bounds].size.height



/**
 *  判断是否是企业证书
 *
 *  @param <
 *
 *  @return
 */
#define kEnterprise [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"] isEqualToString:@"com.blts.iphone"] ? NO : YES
#define Font(a) [UIFont systemFontOfSize:a]

//颜色
#define COLORRGB(a,b,c)           [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:1]
//#define COLORRGB(red,green,blue)  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1]
#define COLOR_Subject_Color COLORRGB(1,64,153)
#define COLOR_XQ_NAV [UIColor colorWithRed:245/255.0 green:246/255.0 blue:247/255.0 alpha:1]
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
#define Color_DefaultBackground COLORRGB(242, 242, 242)











#define User_code [[ UserInfoCache share] userCode]
#define User_accid [[ UserInfoCache share] Accid]









#endif /* UtilConst_h */
