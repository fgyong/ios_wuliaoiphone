//
//  FYBaseViewController.h
//  ios_wuliao
//
//  Created by Charlie on 2017/5/27.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYBaseViewController : UIViewController
/**
 透明的image
 
 @return 返回透明的image
 */
-(UIImage *)cLearImage;

/**
 设置状态栏背景颜色为默认
 */
-(void)setNavigationBarDefault;

/**
 设置状态栏背景颜色为透明
 */
-(void)setNavigationBarCLearBackground;
@property (nonatomic,copy) NSString * titleStr;
-(void)showAlertMessage:(NSString *)message clickBlock:(dispatch_block_t)block;

-(UIImageView *)checkImageViewStatus:(NSInteger)status;
@end
