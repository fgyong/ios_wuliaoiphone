//
//  UtilConstStatic.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/18.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#ifndef UtilConstStatic_h
#define UtilConstStatic_h

static NSString * const kErrorName = @"姓名将涉及后续医疗保险业务,\n为了保证服务质量，请先务必填写真实姓名";
static NSString * const kErrorPhone = @"您的手机号输入有误，请重新输入";
static NSString * const kErrorVerficiCode = @"您的验证码输入有误，请重新输入";
static NSString * const kErrorPwd = @"请输出入字母、数字、下划线组成的密码";
static NSString * const kErrorComplate = @"为保证服务质量，请务必将信息填写完整";
static NSString * const kSendVerCode = @"手机验证码已经发送到手机，请注意查收";
static NSString * const kErrorNetWork = @"网络不好，请稍后重试";
//show的 time
static int  const kDelayTime = 2;

/*默认字体 frame 等 数字定义区*/
static int  const FYDefaultNavigationItemFontSize = 13; /*nav 默认的字体大小*/

#endif /* UtilConstStatic_h */
