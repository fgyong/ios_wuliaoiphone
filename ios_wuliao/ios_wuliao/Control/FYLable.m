//
//  FYLable.m
//  xiaoqu-ios
//
//  Created by Charlie on 15/7/4.
//  Copyright (c) 2015年 meimeidou. All rights reserved.
//

#import "FYLable.h"
#import "UtilConst.h"
#import "MJRefresh.h"

@implementation FYLable
-(instancetype)initWithFrame:(CGRect)frame font:(UIFont *)newFont bgColor:(UIColor *)bgcolor text:(NSString *)newText
{
    return [self initWithFrame:frame font:newFont bgColor:bgcolor text:newText textColor:[UIColor whiteColor]];
}
-(instancetype)initWithFrame:(CGRect)frame font:(UIFont *)newFont bgColor:(UIColor *)bgcolor text:(NSString *)newText textColor:(UIColor *)color
{
    if (self = [super initWithFrame:frame]) {
        self.font =newFont;
        if (bgcolor) {
            self.backgroundColor = bgcolor ;
        }
        else
        {
            self.backgroundColor = [UIColor clearColor] ;
        }
        self.text = newText;
        if (color) {
            self.textColor = color ;
        }
        self.numberOfLines = 0;
        return self ;
    }
    return nil ;
}
+(instancetype) lineWithFrame:(CGRect)frame{
    FYLable *lable=[[FYLable alloc]initWithFrame:frame];
    [lable setMj_h:0.5];
    lable.backgroundColor = COLORRGB(201, 201, 201);
    return lable;
}
@end
