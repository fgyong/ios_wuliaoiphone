//
//  FYDatePickerView.m
//  BLTSZY
//
//  Created by Charlie on 2017/5/17.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYDatePickerView.h"



@interface FYDatePickerView ()
{
    
    UIView *_pickSuperView;
 
}

@end
@implementation FYDatePickerView

-(instancetype)init{
    if (self = [super init]) {
        
        self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        UIView * back=[UIView new];
        back.frame = self.frame;
        back.backgroundColor=[UIColor blackColor];
        back.alpha = 0.3;
        [self addSubview:back];
        
        _pickSuperView=[UIView new];
        _pickSuperView.frame = CGRectMake(0, kScreenHeight, kScreenWidth, 235);
        _pickSuperView.backgroundColor = COLORRGB(237, 237, 237);
        [self addSubview:_pickSuperView];
        UIView * btnBackViw=[UIView new];
        btnBackViw.backgroundColor=COLORRGB(245, 245, 245);
        btnBackViw.frame = CGRectMake(0, 0, kScreenWidth, 45);
        [_pickSuperView addSubview:btnBackViw];
        
        UIButton * cancelBtn=[self btnTitle:NSLocalizedString(@"取消", nil) action:@selector(hide) frame:CGRectMake(12,0,80,45)];
        [btnBackViw addSubview:cancelBtn];
        
        UIButton * saveBtn=[self btnTitle:NSLocalizedString(@"确定", nil) action:@selector(save) frame:CGRectMake(kScreenWidth - 80,0,80,45)];
        [btnBackViw addSubview:saveBtn];
        
        
        
        
        
        self.pickerView=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, 45, kScreenWidth, 190)];
        _pickerView.datePickerMode = UIDatePickerModeDate;
        _pickerView.locale = [NSLocale currentLocale];
        [_pickSuperView addSubview:_pickerView];
    }
    return self;
}
-(UIButton *)btnTitle:(NSString *)title action:(SEL)action frame:(CGRect)rect{
    UIButton * btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = rect;
    [btn setTitle:title forState:UIControlStateNormal];
    btn .titleLabel.font = Font(15);
    [btn setTitleColor:UIColorFromHex(0x157efb) forState:UIControlStateNormal];
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}
-(void)show{
    UIWindow * window=[[UIApplication sharedApplication] keyWindow];
    [window addSubview:self];
    
    [UIView animateWithDuration:0.25 animations:^{
        _pickSuperView.mj_y = kScreenHeight - 235;
    }];
}
-(void)hide{
    [UIView animateWithDuration:0.25 animations:^{
        _pickSuperView.mj_y = kScreenHeight;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}
-(void)save{
    
    if (self.block) {
        NSDateFormatter * format=[NSDateFormatter new];
        [format setDateFormat:@"yyyy-MM-dd"];
        NSString * dateString = [format stringFromDate:_pickerView.date];
        self.block(dateString);
    }
    [self hide];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hide];
}
-(void)selectSuccess:(void (^)(NSString *))block{
    if (block) {
        self.block = block;
    }
}

@end
