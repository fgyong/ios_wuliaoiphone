//
//  MDNavigationController.m
//  nan-ios
//
//  Created by mingshi on 14-8-18.
//  Copyright (c) 2014年 meimeidou. All rights reserved.
//

#import "MDNavigationController.h"
#import <QuartzCore/QuartzCore.h>

@interface MDNavigationController ()<UINavigationControllerDelegate>

@end

@implementation MDNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.navigationBar.translucent = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
//    [[UINavigationBar appearance]setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc]init]];
    
//    if (IS_OS_7_OR_LATER) {
//        [[UINavigationBar appearance]setBarTintColor:COLOR_XQ_NAV];
//    } else {
//        [[UINavigationBar appearance]setTintColor:COLOR_XQ_NAV];
//    }
  
    //if add this panGesture , it will break IOS common slide right back Mingshi 2014-10-19
    //[self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}



#pragma mark -
#pragma mark Gesture recognizer

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    } else {
        viewController.hidesBottomBarWhenPushed = NO;
    }
    [super pushViewController:viewController animated:animated];
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
//     AppDelegate * lication = (AppDelegate *) [[UIApplication sharedApplication] delegate];
//    if (navigationController.viewControllers.count >= 1) {
//        lication.tabbar.tabBar.hidden = YES;
//    }else {
//        lication.tabbar.tabBar.hidden = NO;
//    }
}

@end
