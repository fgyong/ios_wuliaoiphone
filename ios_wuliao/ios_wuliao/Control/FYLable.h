//
//  FYLable.h
//  xiaoqu-ios
//
//  Created by Charlie on 15/7/4.
//  Copyright (c) 2015年 meimeidou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYLable : UILabel
-(instancetype)initWithFrame:(CGRect)frame font:(UIFont *)newFont bgColor:(UIColor *)bgcolor text:(NSString *)newText;
-(instancetype)initWithFrame:(CGRect)frame font:(UIFont *)newFont bgColor:(UIColor *)bgcolor text:(NSString *)newText textColor:(UIColor *)color;
+(instancetype)lineWithFrame:(CGRect)frame;
@end
