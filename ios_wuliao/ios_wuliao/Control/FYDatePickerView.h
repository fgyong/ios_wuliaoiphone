//
//  FYDatePickerView.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/17.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^complateDateBlock)(NSString * dateString);
@interface FYDatePickerView : UIView

@property (nonatomic,strong) UIDatePicker * pickerView;

@property (nonatomic,copy) complateDateBlock block;
-(void)show;
-(void)hide;
-(void)selectSuccess:(void(^)(NSString * dateString))block;;
@end
