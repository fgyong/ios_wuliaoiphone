//
//  FYAdressPickerView.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/17.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^complateBlock)(NSInteger row);
@interface FYPickerView : UIView


@property (nonatomic,strong) NSArray* dataArray;
@property (nonatomic,copy) complateBlock block;
-(void)show;
-(void)hide;
-(void)selectSuccess:(complateBlock)block;
@end
