//
//  FYCheckBox.h
//  BLTSZY
//
//  Created by Charlie on 2017/5/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYCheckBox : UIButton

/**
 设置选择状态

 @param selected nil
 */
-(void)setStatusSelected:(BOOL)selected;
@end
