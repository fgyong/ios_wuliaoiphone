//
//  FYCheckBox.m
//  BLTSZY
//
//  Created by Charlie on 2017/5/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import "FYCheckBox.h"

@implementation FYCheckBox

-(instancetype)init{
    if (self =[super init]) {
        self.backgroundColor= UIColorFromHex(0xe5e5e5);
        self.layer.cornerRadius = 2;
    }
    return self;
}
-(void)setStatusSelected:(BOOL)selected
{
    self.selected = selected;
    if (selected) {
        [self setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    } else {
        [self setImage:nil forState:UIControlStateNormal];
    }
}
@end
