//
//  FYTThirstPickerView.h
//  ios_wuliao
//
//  Created by Charlie on 2017/6/16.
//  Copyright © 2017年 www.fgyong.cn. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^complateBlock)(NSInteger row);
@interface FYTThirstPickerView : UIView


@property (nonatomic,strong) NSArray* dataArray;
@property (nonatomic,copy) complateBlock block;
-(void)show;
-(void)hide;
-(void)selectSuccess:(complateBlock)block;
@end
